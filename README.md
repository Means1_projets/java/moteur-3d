# Moteur 3D - TIPE

TIPE réalisé durant les deux premières années de prépa à CYTech (ex-EISTI), c'est l'aboutissement de 1 an de recherches et un an de développement

Le moteur graphique a été réalisé en Java avec un binding OpenGL, fait avec des techniques comme les VBOs / VBAs ainsi que des shaders (vertexShader & fragmentShader)

Il est composé d'une API (le sous-module api) qui permet de créer des scènes facilement (sous-modules : game / example-jpo) avec l'utilisation de deux fonctions, setup & draw, la création de scène est simplifié

L'API prend en charge automatiquement le chargement des ressources comme les .obj et les textures. Il met aussi à disposition quelques fonctionnalités à rajouter sur une entité, comme une durée de vie, un changement de couleur automatique, une émission de couleur qui change en fonction du temps...



## Images : 

![Model du lapin avec une point light](img/prez2.png)
![Textures](img/prez3.png)
![Flash lights](img/prez1.png)


## Auteurs

Mathieu PAIN

Maxime QUERUEL

Jacque PHELIPPEAU

## Lancer

```bash
./start.sh
``` 
