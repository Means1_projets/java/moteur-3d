#version 430
precision mediump float;
#define NR_POINT_LIGHTS 100

in vec3 normalSurface;
in vec4 FragPos;

struct Light {

    vec3 pos;
    vec3 direction;
    float cutOff;
    float outerCutOff;
    int typeLight;

    vec3 ambient;
    vec3 diffuse;
    vec3 specular;

    float constant;
    float linear;
    float quadratic;
};
struct Material {
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
    float shininess;

    bool lightInfluence;
};

uniform Material material;
uniform Light[NR_POINT_LIGHTS] light;
uniform int nbLight;
uniform vec3 viewPos;

out vec4 out_Color;

vec3 applyLight(Light light){
    vec3 ambient = vec3(0.0);
    vec3 diffuse = vec3(0.0);
    vec3 specular = vec3(0.0);


    if (light.typeLight == 0){


        float distance    = length(light.pos - FragPos.xyz);
        float attenuation = 1.0 / (light.constant + light.linear * distance +
        light.quadratic * (distance * distance));

        ambient  = light.ambient * material.ambient;

        // diffuse
        vec3 norm = normalize(normalSurface);
        vec3 lightDir = normalize(light.pos - FragPos.xyz);
        float diff = max(dot(norm, lightDir), 0.0);
        diffuse  = light.diffuse * (diff * material.diffuse);

        // specular
        vec3 viewDir = normalize(viewPos - FragPos.xyz);
        vec3 reflectDir = reflect(-lightDir, norm);
        float spec = pow(max(dot(viewDir, reflectDir), 0.0), material.shininess);
        specular = light.specular * (spec * material.specular);

        ambient *= attenuation;
        diffuse *= attenuation;
        specular *= attenuation;

    } else if (light.typeLight == 1){

        ambient  = light.ambient * material.ambient;

        // diffuse
        vec3 norm = normalize(normalSurface);
        vec3 lightDir = normalize(-light.direction);
        float diff = max(dot(norm, lightDir), 0.0);
        diffuse  = light.diffuse * (diff * material.diffuse);

        // specular
        vec3 viewDir = normalize(viewPos - FragPos.xyz);
        vec3 reflectDir = reflect(-lightDir, norm);
        float spec = pow(max(dot(viewDir, reflectDir), 0.0), material.shininess);
        specular = light.specular * (spec * material.specular);

    } else if (light.typeLight == 2){
        vec3 lightDir = normalize(light.pos - FragPos.xyz);
        float theta = dot(lightDir, normalize(-light.direction));

        float distance    = length(light.pos - FragPos.xyz);
        float attenuation = 1.0 / (light.constant + light.linear * distance + light.quadratic * (distance * distance));
        if (theta > light.cutOff)
        {
            float epsilon   = light.cutOff - light.outerCutOff;
            float intensity = 1.0 - clamp((theta - light.outerCutOff) / epsilon, 0.0, 1.0);

            ambient  = light.ambient * material.ambient;

            // diffuse
            vec3 norm = normalize(normalSurface);
            float diff = max(dot(norm, lightDir), 0.0);
            diffuse  = light.diffuse * (diff * material.diffuse);

            // specular
            vec3 viewDir = normalize(viewPos - FragPos.xyz);
            vec3 reflectDir = reflect(-lightDir, norm);
            float spec = pow(max(dot(viewDir, reflectDir), 0.0), material.shininess);
            specular = light.specular * (spec * material.specular);
            ambient *= attenuation;
            diffuse *= attenuation;
            specular *= attenuation;
            diffuse *= intensity;
            specular *= intensity;
        }
        else {
            ambient = light.ambient * material.ambient * attenuation;
        }
    }

    return ambient + diffuse + specular;
}

void main(void){


    if (material.lightInfluence){

        vec3 result = vec3(0.0);
        for(int i = 0; i < nbLight; i++) {
            result += applyLight(light[i]);
        }
        out_Color = vec4(result, 1.0);
    } else {
        out_Color = vec4(material.ambient, 1.0);
    }
}