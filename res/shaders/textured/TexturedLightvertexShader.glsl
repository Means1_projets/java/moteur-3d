#version 430
precision mediump float;

in vec3 position;
in vec2 textureCoords;
in vec3 normal;

uniform mat4 transformationMatrix;
uniform mat4 normalMatrix;
uniform mat4 projectionMatrix;
uniform mat4 viewMatrix;
uniform vec3 light_pos;

out vec2 pass_textureCoords;
out vec3 normalSurface;
out vec4 FragPos;

void main(void){
    FragPos = transformationMatrix * vec4(position, 1.0);
    pass_textureCoords = textureCoords;
    normalSurface = (normalMatrix * vec4(normal, 1.0)).xyz;
    gl_Position = projectionMatrix * viewMatrix *  FragPos;
}