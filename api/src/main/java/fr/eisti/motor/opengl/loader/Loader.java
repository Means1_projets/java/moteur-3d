package fr.eisti.motor.opengl.loader;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import fr.eisti.motor.Motor;
import fr.eisti.motor.opengl.entity.Entity;
import fr.eisti.motor.opengl.model.ColoredModel;
import fr.eisti.motor.opengl.model.Model;
import fr.eisti.motor.opengl.model.RawModel;
import fr.eisti.motor.opengl.model.TexturedModel;
import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL15;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;
import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureLoader;

import java.io.*;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Loader {

    private final List<Integer> vaos = new ArrayList<>();
    private final List<Integer> vbos = new ArrayList<>();
    private final List<Integer> textures = new ArrayList<>();
    private final Map<String, Model> models = new HashMap<>();
    private Texture defaultTexture;
    private ObjectMapper objectMapper;
    private Texture defaultSpecular;

    /**
     * Constructeur
     */
    public Loader() {
        init();
    }

    /**
     * Définition des images par défault et load des objects
     */
    public void init() {
        defaultTexture = loadText("default.png");
        defaultSpecular = loadText("defaultspecular.png");
        objectMapper = new ObjectMapper();
        objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
        loadObject();
    }

    /**
     * Enregistrement du model dans la ram
     *
     * @param positions  float[] les pos
     * @param texturepos float[] les pos des textures
     * @param normalpos  float[] normalpos
     * @param indices    int[] list d'indices
     * @return RawModel
     */
    private RawModel loadToVAO(float[] positions, float[] texturepos, float[] normalpos, int[] indices) {

        int vaoID = createVAO();
        if (normalpos == null || normalpos.length == 0) {
            System.out.println("Loader : No Normals Found");
            normalpos = loadNormal(positions);
        }
        storeDataInAttributeList(0, 3, positions);
        storeDataInAttributeList(1, 2, texturepos);
        storeDataInAttributeList(2, 3, normalpos);

        unbindVAO();
        return new RawModel(vaoID, positions.length / 3);
    }

    /**
     * Création des vecteurs normaux s'ils existent pas
     *
     * @param positions
     * @return
     */
    private float[] loadNormal(float[] positions) {
        int i = 0;
        float[] normalArray = new float[positions.length];
        int j = 0;
        while (i < normalArray.length) {
            Vector3f pos1 = new Vector3f(positions[i++], positions[i++], positions[i++]);
            Vector3f pos2 = new Vector3f(positions[i++], positions[i++], positions[i++]);
            Vector3f pos3 = new Vector3f(positions[i++], positions[i++], positions[i++]);
            Vector3f vec1 = new Vector3f(pos2.x - pos1.x, pos2.y - pos1.y, pos2.z - pos1.z);
            Vector3f vec2 = new Vector3f(pos3.x - pos1.x, pos3.y - pos1.y, pos3.z - pos1.z);
            Vector3f v = new Vector3f(vec1.y * vec2.z - vec1.z * vec2.y, vec1.z * vec2.x - vec1.x * vec2.z, vec1.x * vec2.y - vec1.y * vec2.x);
            normalArray[j++] = v.x;
            normalArray[j++] = v.y;
            normalArray[j++] = v.z;
            normalArray[j++] = v.x;
            normalArray[j++] = v.y;
            normalArray[j++] = v.z;
            normalArray[j++] = v.x;
            normalArray[j++] = v.y;
            normalArray[j++] = v.z;
        }
        return normalArray;
    }

    /**
     * Chargement du .obj
     */
    private void loadObject() {
        System.out.println("Loader : Starting...");
        File[] files = new File(Motor.getPath() + "res/objects").listFiles(File::isFile);
        assert files != null;
        int nbMode = files.length;
        int count = 0;

        for (int i = 0; i < files.length; i++) {
            StringBuilder content = new StringBuilder();
            try {
                BufferedReader reader = new BufferedReader(new FileReader(files[i]));
                String line;
                while ((line = reader.readLine()) != null) {
                    content.append(line);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                PreLoadModel model = objectMapper.readValue(content.toString(), PreLoadModel.class);
                loadModel(model);
                System.out.println("Loader : " + model.getName() + " loaded (" + (int) ((((float) i + 1) / (float) nbMode) * 100) + "%)");
                count++;
            } catch (IOException e) {
                System.out.println("Loader : Error with " + files[i]);
            }
        }
        System.out.println("Loader : Finish with " + count + " / " + nbMode + " models");
    }

    /**
     * Debind du model dans la ram
     */
    private void unbindVAO() {
        GL30.glBindVertexArray(0);
    }

    /**
     * Destruction de tous les models
     */
    public void cleanUp() {
        vaos.forEach(GL30::glDeleteVertexArrays);
        vbos.forEach(GL15::glDeleteBuffers);
        textures.forEach(GL11::glDeleteTextures);
    }

    /**
     * Stockage des lists de points (coords) dans la ram
     *
     * @param i
     * @param coordinateSize
     * @param positions
     */
    private void storeDataInAttributeList(int i, int coordinateSize, float[] positions) {
        int vboID = GL15.glGenBuffers();
        vbos.add(vboID);
        GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, vboID);
        FloatBuffer buffer = storeDataInFloatBuffer(positions);
        GL15.glBufferData(GL15.GL_ARRAY_BUFFER, buffer, GL15.GL_STATIC_DRAW);
        GL20.glVertexAttribPointer(i, coordinateSize, GL11.GL_FLOAT, false, 0, 0);
        GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, 0);
    }

    /**
     * Permet de récuperer un FloatBuffer
     *
     * @param positions float[]
     * @return FloatBuffer
     */
    private FloatBuffer storeDataInFloatBuffer(float[] positions) {
        FloatBuffer buffer = BufferUtils.createFloatBuffer(positions.length);
        buffer.put(positions);
        buffer.flip();
        return buffer;
    }

    /**
     * Permet de récuperer un IntBuffer
     *
     * @param data int[]
     * @return IntBuffer
     */
    private IntBuffer storeDataInIntBuffer(int[] data) {
        IntBuffer buffer = BufferUtils.createIntBuffer(data.length);
        buffer.put(data);
        buffer.flip();
        return buffer;
    }

    /**
     * Création du tableau qui sera dans la ram
     *
     * @return int id
     */
    private int createVAO() {
        int vaoID = GL30.glGenVertexArrays();
        vaos.add(vaoID);
        GL30.glBindVertexArray(vaoID);
        return vaoID;
    }

    /**
     * Chargement d'une image
     *
     * @param name
     * @return Texture
     */
    private Texture loadText(String name) {
        Texture texture = null;
        String[] arrayName = name.split(".", -1);
        try {
            //Transformation d'une image sous forme d'un fichier image en un object Texture
            texture = TextureLoader.getTexture(arrayName[1].toUpperCase(), new FileInputStream(Motor.getPath() + "res/objects/textures/" + name));
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (texture == null)
            return defaultTexture;
        textures.add(texture.getTextureID());
        return texture;
    }

    /**
     * Chargement d'un .obj
     *
     * @param fileName
     * @return RawModel
     */
    private RawModel loadObjModel(String fileName) {
        FileReader fileReader;
        try {
            //Lecture du fichier
            fileReader = new FileReader(new File(Motor.getPath() + "res/objects/models/" + fileName));
            BufferedReader reader = new BufferedReader(fileReader);
            String line = null;

            //Initialisation des Arrays.
            List<Vector3f> vertices = new ArrayList<>();
            List<Vector2f> textures = new ArrayList<>();
            List<Vector3f> normals = new ArrayList<>();
            List<Vector3f> vertices2 = new ArrayList<>();
            List<Vector2f> textures2 = new ArrayList<>();
            List<Vector3f> normals2 = new ArrayList<>();
            float[] verticesArray = null;
            float[] normalsArray = null;
            float[] textureArray = null;
            int[] indicesArray = null;

            //Lecture (enregistrement des vertices, vertices de texture, vectices des normals)
            while (true) {
                line = reader.readLine();
                String[] str = line.split(" ");
                if (line.startsWith("v ")) {
                    vertices.add(new Vector3f(Float.parseFloat(str[1]), Float.parseFloat(str[2]), Float.parseFloat(str[3])));
                }
                if (line.startsWith("vt ")) {
                    textures.add(new Vector2f(Float.parseFloat(str[1]), Float.parseFloat(str[2])));
                }
                if (line.startsWith("vn ")) {
                    normals.add(new Vector3f(Float.parseFloat(str[1]), Float.parseFloat(str[2]), Float.parseFloat(str[3])));
                }
                if (line.startsWith("f ")) {
                    textureArray = new float[vertices.size() * 2];
                    normalsArray = new float[vertices.size() * 3];
                    break;
                }
            }
            //Lecture des indices
            while (line != null) {
                if (!line.startsWith("f ")) {
                    line = reader.readLine();
                    continue;
                }
                String[] currentLine = line.split(" ");
                String[] vertex1 = currentLine[1].split("/");
                String[] vertex2 = currentLine[2].split("/");
                String[] vertex3 = currentLine[3].split("/");
                if (vertex1.length != 3) {
                    normalsArray = null;
                }
                if (currentLine.length == 5) {
                    //On remplit les arrays en fonction des indices
                    String[] vertex4 = currentLine[4].split("/");
                    processVertex(vertex1, vertices, vertices2, textures, textures2, normals, normals2);
                    processVertex(vertex2, vertices, vertices2, textures, textures2, normals, normals2);
                    processVertex(vertex4, vertices, vertices2, textures, textures2, normals, normals2);
                    processVertex(vertex4, vertices, vertices2, textures, textures2, normals, normals2);
                    processVertex(vertex2, vertices, vertices2, textures, textures2, normals, normals2);
                    processVertex(vertex3, vertices, vertices2, textures, textures2, normals, normals2);

                } else {
                    processVertex(vertex1, vertices, vertices2, textures, textures2, normals, normals2);
                    processVertex(vertex2, vertices, vertices2, textures, textures2, normals, normals2);
                    processVertex(vertex3, vertices, vertices2, textures, textures2, normals, normals2);

                }
                line = reader.readLine();
            }

            reader.close();

            verticesArray = new float[vertices2.size() * 3];
            textureArray = new float[textures2.size() * 2];
            normalsArray = new float[normals2.size() * 3];

            //Transformation de l'array de Vector3f en une array de float
            int vertexPointer = 0;
            for (Vector3f vertex : vertices2) {
                verticesArray[vertexPointer++] = vertex.x;
                verticesArray[vertexPointer++] = vertex.y;
                verticesArray[vertexPointer++] = vertex.z;
            }
            vertexPointer = 0;
            for (Vector2f vertex : textures2) {
                textureArray[vertexPointer++] = vertex.x;
                textureArray[vertexPointer++] = vertex.y;
            }
            vertexPointer = 0;
            for (Vector3f vertex : normals2) {
                normalsArray[vertexPointer++] = vertex.x;
                normalsArray[vertexPointer++] = vertex.y;
                normalsArray[vertexPointer++] = vertex.z;
            }
            //Chargement dans la ram + récuparation de son id
            return loadToVAO(verticesArray, textureArray, normalsArray, null);
        } catch (IOException err) {
            err.printStackTrace();
        }
        return null;
    }

    //On bien les indices avec les vertices correspondant
    private void processVertex(String[] vertexData, List<Vector3f> vertices, List<Vector3f> vertices2, List<Vector2f> textures, List<Vector2f> textures2, List<Vector3f> normals, List<Vector3f> normals2) {
        int currentVertexPointer = Integer.parseInt(vertexData[0]) - 1;
        vertices2.add(vertices.get(currentVertexPointer));
        if (vertexData.length > 1) {
            Vector2f currentTex = textures.get(Integer.parseInt(vertexData[1]) - 1);
            textures2.add(new Vector2f(currentTex.x, 1 - currentTex.y));
            if (vertexData.length > 2) {
                Vector3f currentNorm = normals.get(Integer.parseInt(vertexData[2]) - 1);
                normals2.add(currentNorm);
            }
        }

    }

    //On créer un ColoredModel ou un TexturedModel en fonction du type du model à partir de ce que l'on a récuperé en json dans le fichier de l'object
    private void loadModel(PreLoadModel preLoadModel) {
        Model model;
        if (preLoadModel.getTexture() == null) {
            //Création d'un coloredmodel
            model = new ColoredModel(loadObjModel(preLoadModel.getModel()), preLoadModel.getColor());
        } else {
            //Création d'un textured model avec une specular map texturé ou non.
            if (preLoadModel.getSpecularMap() != null) {
                model = new TexturedModel(loadObjModel(preLoadModel.getModel()), loadText(preLoadModel.getTexture()), loadText(preLoadModel.getSpecularMap()));
            } else {
                model = new TexturedModel(loadObjModel(preLoadModel.getModel()), loadText(preLoadModel.getTexture()), defaultSpecular);
            }
        }
        //Définition de certaines carractéristiques
        model.setName(preLoadModel.getName());
        model.setLight(preLoadModel.getLight());
        model.setLigthInfluence(preLoadModel.isLigthInfluence());
        model.setReflectivity(preLoadModel.getReflectivity());
        model.setShineDamper(preLoadModel.getShineDamper());
        models.put(preLoadModel.getKey(), model);
    }

    /**
     * Création d'une entité à partir d'un model
     *
     * @param name String model
     * @param x    float position x
     * @param y    float position y
     * @param z    float position z
     * @param rx   float direction x
     * @param ry   float direction y
     * @param rz   float direction z
     * @param size float size
     * @return Entity
     */
    public Entity loadEntity(String name, float x, float y, float z, float rx, float ry, float rz, float size) {
        Model model = models.get(name);
        if (model == null) {
            System.out.println("Error : Model inconnu : " + name + " ");
            return null;
        }
        return new Entity(model, x, y, z, rx, ry, rz, size);
    }

}
