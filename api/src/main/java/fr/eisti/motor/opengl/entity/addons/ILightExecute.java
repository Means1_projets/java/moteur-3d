package fr.eisti.motor.opengl.entity.addons;

import fr.eisti.motor.opengl.shader.RawModelShader;

public interface ILightExecute {
    void execute(RawModelShader shader, int i);
}
