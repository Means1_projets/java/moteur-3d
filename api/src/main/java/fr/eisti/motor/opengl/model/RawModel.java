package fr.eisti.motor.opengl.model;

import lombok.Getter;

public class RawModel {

    @Getter
    private final int vaoID;
    @Getter
    private final int vertexCount;

    /**
     * Constructeur
     *
     * @param vaoID       int
     * @param vertexCount int
     */
    public RawModel(int vaoID, int vertexCount) {
        this.vaoID = vaoID;
        this.vertexCount = vertexCount;
    }

}
