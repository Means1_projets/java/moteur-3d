package fr.eisti.motor.opengl.entity.addons;

public class LifeSpan extends EntityAddons implements IAfterRender {
    private final int lifespan;
    private int actual = 0;

    public LifeSpan(int i) {
        this.lifespan = i;
    }

    /**
     * Destroy quand le lifespan est atteint
     */
    @Override
    public void execute() {
        if (this.actual++ > lifespan) {
            getEntity().destroy();
        }
    }
}
