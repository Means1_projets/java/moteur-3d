package fr.eisti.motor.opengl.engine;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import fr.eisti.motor.Motor;
import fr.eisti.motor.opengl.entity.Entity;
import fr.eisti.motor.opengl.entity.addons.IAfterRender;
import fr.eisti.motor.opengl.entity.addons.ICommon;
import fr.eisti.motor.opengl.light.Light;
import fr.eisti.motor.opengl.model.Model;
import fr.eisti.motor.opengl.shader.ColoredShader;
import fr.eisti.motor.opengl.shader.RawModelShader;
import fr.eisti.motor.opengl.shader.TexturedShader;
import lombok.Getter;
import lombok.Setter;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector3f;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Renderer {

    private static final float FOV = 70;
    private static final float NEAR_PLANE = 0.1f;
    private static final float FAR_PLANE = 1000;
    @Getter
    private final Multimap<Model, Entity> entitys = ArrayListMultimap.create();
    @Getter
    private final ArrayList<Light> lights = new ArrayList<>();
    private Matrix4f projectionMatrix;
    @Setter
    @Getter
    private Vector3f colorFond = new Vector3f(245F / 255f, 245F / 255f, 220F / 255f);


    public Renderer() {
        initProjection();
    }

    /**
     * Clear de l'écran
     */
    private void prepare() {
        GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT);
        GL11.glClearColor(colorFond.x, colorFond.y, colorFond.z, 1);
    }

    /**
     * Création de la matrice de projection.
     */
    private void creationProjectionMatrix() {
        float fAspectRatio = ((float) Display.getWidth()) / ((float) Display.getHeight());
        float y_scale = (float) ((1.0F / Math.tan(Math.toRadians(FOV / 2f))) * fAspectRatio);
        float x_scale = y_scale / fAspectRatio;
        float length = FAR_PLANE - NEAR_PLANE;
        projectionMatrix = new Matrix4f();
        projectionMatrix.m00 = x_scale;
        projectionMatrix.m11 = y_scale;
        projectionMatrix.m22 = -((FAR_PLANE + NEAR_PLANE) / length);
        projectionMatrix.m23 = -1;
        projectionMatrix.m32 = -((2 * NEAR_PLANE * FAR_PLANE) / length);
        projectionMatrix.m33 = 0;
    }


    /**
     * Met en place la matrice de Projection dans les shaders
     */
    public void initProjection() {
        creationProjectionMatrix();
        Motor.getInstance().getColoredShader().start();
        Motor.getInstance().getColoredShader().loadProjectionMatrix(projectionMatrix);
        Motor.getInstance().getColoredShader().stop();
        Motor.getInstance().getTexturedShader().start();
        Motor.getInstance().getTexturedShader().loadProjectionMatrix(projectionMatrix);
        Motor.getInstance().getTexturedShader().stop();
    }

    /**
     * Affiche les entitées
     */
    public void render() {

        //clear
        prepare();


        //On remove les entités qui ne sont plus en vie
        entitys.entries().removeIf(p -> p.getValue().isDead());


        //On récupère les Lights
        List<Entity> entitywithLight = entitys.entries().stream().filter(p -> p.getKey().getLight() != null).map(Map.Entry::getValue).collect(Collectors.toList());


        //Mise en place des lights dans les shaders
        ColoredShader shader2 = Motor.getInstance().getColoredShader();
        shader2.start();
        shader2.loadLight(entitywithLight, lights);
        shader2.stop();
        TexturedShader shader3 = Motor.getInstance().getTexturedShader();
        shader3.start();
        shader3.loadLight(entitywithLight, lights);
        shader3.stop();

        //Pour chaque model
        for (Map.Entry<Model, Collection<Entity>> entry : entitys.asMap().entrySet()) {
            if (entry.getValue().size() != 0) {
                Model model = entry.getKey();
                Collection<Entity> value = entry.getValue();
                RawModelShader shader = model.getShader();
                //On démarre le shader
                shader.start();

                //On met la matrice de la caméra dans le shader
                shader.loadViewMatrix(Motor.getInstance().getCamera().createViewMatrix(), Motor.getInstance().getCamera().getPos());

                //Bind du model avec le shader
                GL30.glBindVertexArray(model.getRawModel().getVaoID());


                shader.loadShine(model.getShineDamper());

                //Bind des vertices, textPos, Normals
                GL20.glEnableVertexAttribArray(0);
                GL20.glEnableVertexAttribArray(1);
                GL20.glEnableVertexAttribArray(2);


                model.prepareRender();

                //Pour chaque entité avec le model
                for (Entity entity : value) {
                    //Call des addons
                    entity.getAddons().stream().filter(p -> p instanceof ICommon).forEach(p -> ((ICommon) p).execute(shader));
                    //Bind de la matrice de transformation dans le shader
                    shader.loadTransformationMatrix(entity.createTransformation());

                    //On appelle le shader
                    GL11.glDrawArrays(GL11.GL_TRIANGLES, 0, model.getRawModel().getVertexCount());
                }
                //Debind
                GL20.glDisableVertexAttribArray(0);
                GL20.glDisableVertexAttribArray(1);
                GL20.glDisableVertexAttribArray(2);
                GL30.glBindVertexArray(0);
                shader.stop();
            }
        }
        //Call des addons
        entitys.values().stream().map(Entity::getAddons).filter(p -> p instanceof IAfterRender).forEach(a -> ((IAfterRender) a).execute());
    }

}
