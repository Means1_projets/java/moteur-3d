package fr.eisti.motor.opengl.model;

import fr.eisti.motor.opengl.light.Light;
import fr.eisti.motor.opengl.shader.RawModelShader;
import lombok.Getter;
import lombok.Setter;

public abstract class Model {
    @Getter
    @Setter
    private RawModel rawModel;
    @Getter
    @Setter
    private String name;
    @Getter
    @Setter
    private float shineDamper = 1;
    @Getter
    @Setter
    private float reflectivity = 0;
    @Getter
    @Setter
    private boolean ligthInfluence = true;
    @Getter
    @Setter
    private Light light;


    /**
     * Constructeur
     *
     * @param rawModel RawModel
     */
    Model(RawModel rawModel) {
        this.rawModel = rawModel;
    }

    public abstract RawModelShader getShader();

    public abstract void prepareRender();

}
