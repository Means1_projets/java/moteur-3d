package fr.eisti.motor.opengl.model;

import fr.eisti.motor.Motor;
import fr.eisti.motor.opengl.loader.ColorADS;
import fr.eisti.motor.opengl.shader.ColoredShader;
import lombok.Getter;

public class ColoredModel extends Model {


    @Getter
    private final ColorADS color;

    /**
     * Constructeur
     *
     * @param rawModel RawModel
     * @param textID   ColorADS
     */
    public ColoredModel(RawModel rawModel, ColorADS textID) {
        super(rawModel);
        this.color = textID;
    }

    /**
     * Récupération du shader
     *
     * @return ColoredShader
     */
    @Override
    public ColoredShader getShader() {
        return Motor.getInstance().getColoredShader();
    }


    /**
     * Call avant d'affiché
     */
    @Override
    public void prepareRender() {
        ColoredShader coloredShader = getShader();
        coloredShader.setLightInfluence(isLigthInfluence());
        coloredShader.loadColor(color.getAmbient(), color.getDiffuse(), color.getSpecular());
    }
}
