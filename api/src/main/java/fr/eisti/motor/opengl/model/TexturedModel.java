package fr.eisti.motor.opengl.model;

import fr.eisti.motor.Motor;
import fr.eisti.motor.opengl.shader.TexturedShader;
import lombok.Getter;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;
import org.newdawn.slick.opengl.Texture;

import static org.lwjgl.opengl.GL11.GL_TEXTURE_2D;

public class TexturedModel extends Model {


    @Getter
    private final Texture textID;
    @Getter
    private final Texture specularID;


    /**
     * Constructeur du TexturedModel
     *
     * @param rawModel   RawModel
     * @param textID     Texture
     * @param specularID Texture
     */
    public TexturedModel(RawModel rawModel, Texture textID, Texture specularID) {
        super(rawModel);
        this.textID = textID;
        this.specularID = specularID;
    }

    /**
     * Récupartion du shader
     *
     * @return TexturedShader
     */
    @Override
    public TexturedShader getShader() {
        return Motor.getInstance().getTexturedShader();
    }

    /**
     * Call avant d'afficher l'entité
     */
    @Override
    public void prepareRender() {
        TexturedShader shader = getShader();
        shader.setLightInfluence(isLigthInfluence());
        shader.loadTexture();

        GL13.glActiveTexture(GL13.GL_TEXTURE0);
        GL11.glBindTexture(GL_TEXTURE_2D, textID.getTextureID());
        GL13.glActiveTexture(GL13.GL_TEXTURE1);
        GL11.glBindTexture(GL_TEXTURE_2D, specularID.getTextureID());
    }
}
