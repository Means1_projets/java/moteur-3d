package fr.eisti.motor.opengl.entity.addons;

import fr.eisti.motor.opengl.light.Light;
import fr.eisti.motor.opengl.loader.ColorADS;
import fr.eisti.motor.opengl.shader.RawModelShader;
import org.lwjgl.util.vector.Vector3f;

import java.util.Random;

public class RainbowLightAddons extends EntityAddons implements ILightExecute {

    private final Random random = new Random();
    private final float a = random.nextFloat() * 10 - 10;
    private final float b = random.nextFloat() * 10 - 10;
    private final float c = random.nextFloat() * 10 - 10;
    private float i = 0;
    private Light light;

    public RainbowLightAddons() {

    }

    /**
     * Envoie au shader une couleur de light random
     *
     * @param shader
     * @param j
     */
    @Override
    public void execute(RawModelShader shader, int j) {
        if (light == null) {
            light = getEntity().getModel().getLight().clone();
        }
        Vector3f color = new Vector3f((float) Math.sin(i * a) / 2F + 0.5F, (float) Math.sin(i * b) / 2F + 0.5F, (float) Math.cos(i * c) / 2F + 0.5F);
        light.setColor(new ColorADS(new Vector3f(), (Vector3f) new Vector3f(color).scale(0.2F), new Vector3f(color)));
        shader.loadLight(getEntity(), light, j);
        i += Math.PI / 200;
    }
}
