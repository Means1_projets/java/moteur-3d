package fr.eisti.motor.opengl.shader;

public class TexturedShader extends RawModelShader {

    private static final String VERTEX_FILE = "res/shaders/textured/TexturedLightvertexShader.glsl";
    private static final String FRAGMENT_FILE = "res/shaders/textured/TexturedLightfragmentShader.glsl";
    private int material_diffuse;
    private int material_specularMap;

    public TexturedShader() {
        super(VERTEX_FILE, FRAGMENT_FILE);
    }

    /**
     * On récupère l'id des variable qui sont dans le shader
     */
    @Override
    protected void getAllUniformLocations() {
        super.getAllUniformLocations();
        material_diffuse = super.getUniformLocation("material.diffuse");
        material_specularMap = super.getUniformLocation("material.specularMap");
    }

    /**
     * Chargement de la texture dans le shader
     */
    public void loadTexture() {
        super.loadInt(material_diffuse, 0);
        super.loadInt(material_specularMap, 1);
    }

}
