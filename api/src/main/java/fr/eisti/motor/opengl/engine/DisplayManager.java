package fr.eisti.motor.opengl.engine;

import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.*;

public class DisplayManager {

    private static final int WIDTH = 1000;
    private static final int HEIGHT = 700;
    private static final int FPS_CAP = 60;

    /**
     * Création de la fenêtre.
     */
    public static void createDisplay() {
        ContextAttribs attribs = new ContextAttribs(3, 2).withForwardCompatible(true).withProfileCore(true);
        try {
            Display.setDisplayMode(new DisplayMode(WIDTH, HEIGHT));

            Display.create(new PixelFormat(), attribs);
            Display.setTitle("Moteur 3D : ?");
            Display.setResizable(true);
        } catch (LWJGLException e) {
            e.printStackTrace();
        }

        GL11.glViewport(0, 0, WIDTH, HEIGHT);
    }

    /**
     * Update de l'écran
     */
    public static void updateDisplay() {
        Display.update();
    }

    /**
     * Ferme la fenêtre
     */
    public static void closeDisplay() {
        Display.destroy();
    }
}
