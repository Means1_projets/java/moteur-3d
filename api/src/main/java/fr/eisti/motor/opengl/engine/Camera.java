package fr.eisti.motor.opengl.engine;

import fr.eisti.motor.Motor;
import lombok.Getter;
import lombok.Setter;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector3f;

public class Camera {

    @Getter
    @Setter
    private Vector3f pos = new Vector3f(0, 0, 0);
    private float pitch = 0;
    private float yaw = 0;
    private boolean grab = true;
    private boolean ctrlr, ctrls;

    /**
     * Constructeur de la class Caméra
     */
    public Camera() {
        Mouse.setCursorPosition(Display.getWidth() / 2, Display.getHeight() / 2);
    }

    /**
     * Retourne la Matrice qui est la ViewMatrix
     *
     * @return Matrix4f
     */
    public Matrix4f createViewMatrix() {
        Matrix4f matrix4f = new Matrix4f();
        matrix4f.setIdentity();
        matrix4f.rotate((float) Math.toRadians(pitch), new Vector3f(1, 0, 0));
        matrix4f.rotate((float) Math.toRadians(yaw), new Vector3f(0, 1, 0));
        matrix4f.translate(new Vector3f(-pos.x, -pos.y, -pos.z));
        return matrix4f;
    }

    /**
     * Retourne le vecteur qui est la direction dans laquelle le joueur regarde
     *
     * @return
     */
    public Vector3f getDir() {
        float yaw = this.yaw + 90;
        float pitch = this.pitch + 180;
        return new Vector3f((float) (Math.cos(Math.toRadians(yaw)) * Math.cos(Math.toRadians(pitch))), (float) Math.sin(Math.toRadians(pitch)), (float) (Math.cos(Math.toRadians(pitch)) * Math.sin(Math.toRadians(yaw))));
    }

    /**
     * Update de la caméra quand une action est faite (keys)
     */
    public void update() {
        //Si la touche Z est enfoncée, on bouge la caméra
        if (Keyboard.isKeyDown(Keyboard.KEY_Z)) {
            pos.translate((float) (-0.1f * Math.cos(Math.toRadians(yaw + 90))), 0, (float) (-0.1f * Math.sin(Math.toRadians(yaw + 90))));
        }
        if (Keyboard.isKeyDown(Keyboard.KEY_S)) {
            pos.translate((float) (0.1f * Math.cos(Math.toRadians(yaw + 90))), 0, (float) (0.1f * Math.sin(Math.toRadians(yaw + 90))));
        }
        if (Keyboard.isKeyDown(Keyboard.KEY_Q)) {
            pos.translate((float) (-0.1f * Math.cos(Math.toRadians(yaw))), 0, (float) (-0.1f * Math.sin(Math.toRadians(yaw))));
        }
        if (Keyboard.isKeyDown(Keyboard.KEY_D)) {
            pos.translate((float) (0.1f * Math.cos(Math.toRadians(yaw))), 0, (float) (0.1f * Math.sin(Math.toRadians(yaw))));
        }
        if (Keyboard.isKeyDown(Keyboard.KEY_SPACE)) {
            pos.translate(0, 0.1f, 0);
        }
        if (Keyboard.isKeyDown(Keyboard.KEY_LSHIFT))
            pos.translate(0, -0.1f, 0);

        // Escape de la caméra
        if (Keyboard.isKeyDown(Keyboard.KEY_ESCAPE) && grab) {
            Mouse.setGrabbed(grab = false);
        }

        if (Mouse.isButtonDown(0) && !grab) {
            grab = true;
        }
        if (grab) {
            float speed = 0.2f;
            yaw = (yaw + ((float) Mouse.getDX()) * speed) % 360;
            float pitch2 = ((float) Mouse.getDY()) * speed;
            if (pitch - pitch2 > 90) {
                pitch = 90;
            } else if (pitch - pitch2 < -90) {
                pitch = -90;
            } else {
                pitch -= pitch2;
            }
            Mouse.setGrabbed(true);
        }


        // Ctrl + s -> reload shader
        if (Keyboard.isKeyDown(Keyboard.KEY_S) && Keyboard.isKeyDown(Keyboard.KEY_LCONTROL)) {
            if (!ctrls) {
                ctrls = true;
                System.out.println("Reload Shaders...");
                Motor.getInstance().getTexturedShader().reload();
                Motor.getInstance().getColoredShader().reload();
                System.out.println("Reloaded.");
            }
        } else {
            ctrls = false;
        }

        // Ctrl + r -> reload
        if (Keyboard.isKeyDown(Keyboard.KEY_R) && Keyboard.isKeyDown(Keyboard.KEY_LCONTROL)) {
            if (!ctrlr) {
                ctrlr = true;
                System.out.println("Reload Models...");
                Motor.getInstance().getLoader().cleanUp();
                Motor.getInstance().getRenderer().getEntitys().clear();
                Motor.getInstance().getLoader().init();
                Motor.getInstance().setup();
                System.out.println("Reload Shaders...");
                Motor.getInstance().getTexturedShader().reload();
                Motor.getInstance().getColoredShader().reload();
                System.out.println("Reloaded.");
            }
        } else {
            ctrlr = false;
        }
    }
}
