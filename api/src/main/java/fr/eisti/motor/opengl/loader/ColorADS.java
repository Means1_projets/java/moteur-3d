package fr.eisti.motor.opengl.loader;

import lombok.Getter;
import lombok.Setter;
import org.lwjgl.util.vector.Vector3f;

public class ColorADS {
    @Getter
    @Setter
    private Vector3f ambient;
    @Getter
    @Setter
    private Vector3f diffuse;
    @Getter
    @Setter
    private Vector3f specular;

    /**
     * Constructeur pour json
     */
    public ColorADS() {

    }

    /**
     * Constructeur
     *
     * @param ambient  Vector3f ambient
     * @param diffuse  Vector3f diffuse
     * @param specular Vector3f specular
     */
    public ColorADS(Vector3f ambient, Vector3f diffuse, Vector3f specular) {

        this.ambient = ambient;
        this.diffuse = diffuse;
        this.specular = specular;
    }

}
