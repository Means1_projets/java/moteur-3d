package fr.eisti.motor.opengl.shader;

import fr.eisti.motor.Motor;
import fr.eisti.motor.opengl.entity.Entity;
import fr.eisti.motor.opengl.light.Light;
import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL20;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.FloatBuffer;
import java.util.List;

public abstract class AbstractShader {

    private static final FloatBuffer matrixBuffer = BufferUtils.createFloatBuffer(16);
    private final String vertexFile;
    private final String fragmentFile;
    private int programID;
    private int vertexShaderID;
    private int fragmentShaderID;

    /**
     * Constructeur du Shader
     *
     * @param vertexFile   String
     * @param fragmentFile String
     */
    AbstractShader(String vertexFile, String fragmentFile) {
        this.vertexFile = vertexFile;
        this.fragmentFile = fragmentFile;
        load();
    }

    /**
     * Envoie du shader à un compilateur
     *
     * @param file
     * @param type
     * @return
     */
    private static int loadShader(String file, int type) {
        StringBuilder shaderSource = new StringBuilder();
        try {
            BufferedReader reader = new BufferedReader(new FileReader(Motor.getPath() + file));
            String line;
            while ((line = reader.readLine()) != null) {
                shaderSource.append(line).append("\n");
            }
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(-1);
        }
        int shaderID = GL20.glCreateShader(type);
        GL20.glShaderSource(shaderID, shaderSource);
        GL20.glCompileShader(shaderID);
        if (GL20.glGetShaderi(shaderID, GL20.GL_COMPILE_STATUS) == GL11.GL_FALSE) {
            System.out.println(file + " : " + GL20.glGetShaderInfoLog(shaderID, 500));
        }
        return shaderID;
    }

    /**
     * Chargement des shaders
     */
    private void load() {
        vertexShaderID = loadShader(vertexFile, GL20.GL_VERTEX_SHADER);
        fragmentShaderID = loadShader(fragmentFile, GL20.GL_FRAGMENT_SHADER);
        programID = GL20.glCreateProgram();
        GL20.glAttachShader(programID, vertexShaderID);
        GL20.glAttachShader(programID, fragmentShaderID);
        bindAttributes();
        GL20.glLinkProgram(programID);
        GL20.glValidateProgram(programID);
        getAllUniformLocations();
    }

    protected abstract void getAllUniformLocations();


    int getUniformLocation(String uniformName) {
        return GL20.glGetUniformLocation(programID, uniformName);
    }

    /**
     * Lancement du shader
     */
    public void start() {
        GL20.glUseProgram(programID);
    }

    /**
     * Arret du shader
     */
    public void stop() {
        GL20.glUseProgram(0);
    }

    /**
     * Suppression du shader
     */
    private void cleanUp() {
        stop();
        GL20.glDetachShader(programID, vertexShaderID);
        GL20.glDetachShader(programID, fragmentShaderID);
        GL20.glDeleteShader(vertexShaderID);
        GL20.glDeleteShader(fragmentShaderID);
        GL20.glDeleteProgram(programID);

    }

    protected abstract void bindAttributes();

    /**
     * On link le contenue dans la ram avec une variable
     *
     * @param attribute
     * @param variableName
     */
    void bindAttribute(int attribute, String variableName) {
        GL20.glBindAttribLocation(programID, attribute, variableName);
    }

    /**
     * Envoie d'un Float (variable) dans le shader
     *
     * @param location
     * @param value
     */
    void loadFloat(int location, float value) {
        GL20.glUniform1f(location, value);
    }

    /**
     * Envoie d'un Vector3f (variable) dans le shader
     *
     * @param location
     * @param vector3f
     */
    void loadVector3f(int location, Vector3f vector3f) {
        GL20.glUniform3f(location, vector3f.x, vector3f.y, vector3f.z);
    }

    /**
     * Envoie d'un Vector2f (variable) dans le shader
     *
     * @param location
     * @param vector2f
     */
    protected void loadVector2f(int location, Vector2f vector2f) {
        GL20.glUniform2f(location, vector2f.x, vector2f.y);
    }

    /**
     * Envoie d'un boolean (variable) dans le shader
     *
     * @param location
     * @param value
     */
    void loadBoolean(int location, boolean value) {
        float toLoad = 0;
        if (value) {
            toLoad = 1;
        }
        GL20.glUniform1f(location, toLoad);
    }

    /**
     * Envoie d'une matrice (variable) dans le shader
     *
     * @param location
     * @param matrix
     */
    void loadMatrix(int location, Matrix4f matrix) {
        matrix.store(matrixBuffer);
        matrixBuffer.flip();
        GL20.glUniformMatrix4(location, false, matrixBuffer);
    }


    public abstract void setLightInfluence(boolean lightInfluence2);

    public abstract void loadLight(List<Entity> entities, List<Light> lights);

    public abstract void loadShine(float shiny);

    /**
     * Reload le shader
     */
    public void reload() {
        cleanUp();
        load();
        Motor.getInstance().getRenderer().initProjection();
    }

    /**
     * Envoie d'un int (variable) dans le shader
     *
     * @param id
     * @param i
     */
    void loadInt(int id, int i) {
        GL20.glUniform1i(id, i);
    }
}
