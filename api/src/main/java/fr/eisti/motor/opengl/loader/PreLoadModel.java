package fr.eisti.motor.opengl.loader;

import fr.eisti.motor.opengl.light.Light;
import lombok.Getter;
import lombok.Setter;

class PreLoadModel {
    @Getter
    @Setter
    private String key = null;
    @Getter
    @Setter
    private String name = null;
    @Getter
    @Setter
    private String model = null;
    @Getter
    @Setter
    private String texture = null;
    @Getter
    @Setter
    private String specularMap = null;
    @Getter
    @Setter
    private ColorADS color = null;
    @Getter
    @Setter
    private float shineDamper = 1;
    @Getter
    @Setter
    private float reflectivity = 0;
    @Getter
    @Setter
    private boolean ligthInfluence = true;
    @Getter
    @Setter
    private Light light = null;


    /**
     * Constructeur call par json
     */
    public PreLoadModel() {

    }
}
