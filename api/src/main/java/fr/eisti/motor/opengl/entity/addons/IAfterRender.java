package fr.eisti.motor.opengl.entity.addons;

public interface IAfterRender {

    void execute();
}
