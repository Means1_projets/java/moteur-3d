package fr.eisti.motor.opengl.shader;

import fr.eisti.motor.opengl.entity.Entity;
import fr.eisti.motor.opengl.entity.addons.ILightExecute;
import fr.eisti.motor.opengl.light.Light;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector3f;

import java.util.List;

public abstract class RawModelShader extends AbstractShader {


    private int location_transformationMatrix;
    private int location_projectionMatrix;
    private int location_viewmatrix;
    private int location_normalMatrix;
    private int viewPos;


    private int material_lightInfluence;
    private int material_shineDamper;
    private int nbLight;

    RawModelShader(String vertexFile, String fragmentFile) {
        super(vertexFile, fragmentFile);
    }

    /**
     * On récupère l'id des variable qui sont dans le shader
     */
    @Override
    protected void getAllUniformLocations() {
        location_transformationMatrix = super.getUniformLocation("transformationMatrix");
        location_projectionMatrix = super.getUniformLocation("projectionMatrix");
        location_normalMatrix = super.getUniformLocation("normalMatrix");
        location_viewmatrix = super.getUniformLocation("viewMatrix");
        viewPos = super.getUniformLocation("viewPos");

        material_lightInfluence = super.getUniformLocation("material.lightInfluence");
        material_shineDamper = super.getUniformLocation("material.shininess");

        nbLight = super.getUniformLocation("nbLight");
    }

    /**
     * Bind des coordonnées avec le shader
     */
    @Override
    protected void bindAttributes() {
        super.bindAttribute(0, "position");
        super.bindAttribute(1, "textureCoords");
        super.bindAttribute(2, "normal");
    }

    /**
     * Chargement de la matrice de Transformation dans le shader
     *
     * @param matrix4f
     */
    public void loadTransformationMatrix(Matrix4f matrix4f) {
        super.loadMatrix(location_transformationMatrix, matrix4f);
        Matrix4f matrix4f2 = new Matrix4f(matrix4f);
        matrix4f2.invert().transpose();
        super.loadMatrix(location_normalMatrix, matrix4f2);
    }

    /**
     * Chargement de la matrice de Projection dans le shader
     *
     * @param ProjectionMatrix
     */
    public void loadProjectionMatrix(Matrix4f ProjectionMatrix) {
        super.loadMatrix(location_projectionMatrix, ProjectionMatrix);
    }

    /**
     * Chargement de la matrice de la caméra dans le shader
     *
     * @param viewMatrix
     * @param vector3f
     */
    public void loadViewMatrix(Matrix4f viewMatrix, Vector3f vector3f) {
        super.loadMatrix(location_viewmatrix, viewMatrix);
        super.loadVector3f(viewPos, vector3f);
    }

    /**
     * Mise en place de la light qui est attaché à une entité
     *
     * @param entity Entity
     * @param light  Light
     * @param i      int
     */
    public void loadLight(Entity entity, Light light, int i) {
        Matrix4f matrix4f = entity.createTransformation();
        Vector3f pos = multiply(matrix4f, light.getPos()).translate(entity.getPos().x, entity.getPos().y, entity.getPos().z);
        Vector3f direction = multiply(matrix4f, light.getPos());
        loadLight(light, i, pos, direction);
    }

    /**
     * Envoie de la ième light dans la shader
     *
     * @param light
     * @param i
     * @param pos
     * @param direction
     */
    public void loadLight(Light light, int i, Vector3f pos, Vector3f direction) {
        super.loadVector3f(super.getUniformLocation("light[" + i + "].pos"), pos);
        super.loadVector3f(super.getUniformLocation("light[" + i + "].direction"), direction);
        super.loadVector3f(super.getUniformLocation("light[" + i + "].ambient"), light.getColor().getAmbient());
        super.loadVector3f(super.getUniformLocation("light[" + i + "].diffuse"), light.getColor().getDiffuse());
        super.loadVector3f(super.getUniformLocation("light[" + i + "].specular"), light.getColor().getSpecular());
        super.loadFloat(super.getUniformLocation("light[" + i + "].constant"), light.getConstant());
        super.loadFloat(super.getUniformLocation("light[" + i + "].linear"), light.getLinear());
        super.loadFloat(super.getUniformLocation("light[" + i + "].quadratic"), light.getQuadratic());
        super.loadInt(super.getUniformLocation("light[" + i + "].typeLight"), light.getLightType().getId());
        super.loadFloat(super.getUniformLocation("light[" + i + "].cutOff"), light.getCutOff());
        super.loadFloat(super.getUniformLocation("light[" + i + "].outerCutOff"), light.getOuterCutOff());
    }

    /**
     * Mise en place des lumières dans le shader
     *
     * @param entities
     * @param lights
     */
    @Override
    public void loadLight(List<Entity> entities, List<Light> lights) {
        int i;
        for (i = 0; i < lights.size(); i++) {
            Light light = lights.get(i);
            loadLight(light, i, light.getPos(), light.getDir());
        }
        for (Entity entity : entities) {
            Light light = entity.getModel().getLight();
            loadLight(entity, light, i);
            int finalI = i;
            entity.getAddons().stream().filter(p -> p instanceof ILightExecute).forEach(p -> ((ILightExecute) p).execute(this, finalI));
            i++;


        }
        super.loadInt(nbLight, lights.size() + entities.size());
    }

    private Vector3f multiply(Matrix4f matrix4f, Vector3f pos) {
        return new Vector3f(matrix4f.m00 * pos.x + matrix4f.m10 * pos.y + matrix4f.m20 * pos.z, matrix4f.m01 * pos.x + matrix4f.m11 * pos.y + matrix4f.m21 * pos.z, matrix4f.m02 * pos.x + matrix4f.m12 * pos.y + matrix4f.m22 * pos.z);
    }

    /**
     * Mise en place du boolean LightInfluence dans le shader
     *
     * @param lightInfluence2
     */
    @Override
    public void setLightInfluence(boolean lightInfluence2) {
        super.loadBoolean(material_lightInfluence, lightInfluence2);
    }

    /**
     * Mise en place de la réflection dans le shader
     *
     * @param shiny
     */
    @Override
    public void loadShine(float shiny) {
        super.loadFloat(material_shineDamper, shiny);
    }
}
