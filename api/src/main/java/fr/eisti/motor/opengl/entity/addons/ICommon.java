package fr.eisti.motor.opengl.entity.addons;

import fr.eisti.motor.opengl.shader.RawModelShader;

public interface ICommon {

    void execute(RawModelShader shader);
}
