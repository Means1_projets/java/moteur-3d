package fr.eisti.motor.opengl.entity;

import fr.eisti.motor.Motor;
import fr.eisti.motor.opengl.entity.addons.EntityAddons;
import fr.eisti.motor.opengl.model.Model;
import lombok.Getter;
import lombok.Setter;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector3f;

import java.util.ArrayList;
import java.util.List;

public class Entity {

    private static int idCount;
    @Getter
    private final Model model;
    @Getter
    private final int id;
    @Getter
    private final List<EntityAddons> addons = new ArrayList<>();
    @Getter
    @Setter
    private Vector3f rot;
    @Getter
    @Setter
    private Vector3f pos;
    @Getter
    @Setter
    private float size;
    @Getter
    @Setter
    private boolean alive = true;

    /**
     * Initialisation
     *
     * @param model Model - Le model
     * @param x     float - Position
     * @param y     float - Position
     * @param z     float - Position
     * @param rx    float - Rotation
     * @param ry    float - Rotation
     * @param rz    float - Rotation
     * @param size  float - Taille
     */
    public Entity(Model model, float x, float y, float z, float rx, float ry, float rz, float size) {
        this.model = model;
        this.pos = new Vector3f(x, y, z);
        this.rot = new Vector3f(rx, ry, rz);
        this.size = size;
        this.id = idCount++;
        Motor.getInstance().getRenderer().getEntitys().put(model, this);
    }

    /**
     * Modification de la position
     *
     * @param dx
     * @param dy
     * @param dz
     */
    public void increasePosition(float dx, float dy, float dz) {
        this.pos.translate(dx, dy, dz);
    }

    /**
     * Modification de la rotation
     *
     * @param dx
     * @param dy
     * @param dz
     */
    public void increaseRotation(float dx, float dy, float dz) {
        this.rot.translate(dx, dy, dz);
    }


    /**
     * Création de la matrice de transformation
     *
     * @return
     */
    public Matrix4f createTransformation() {
        Matrix4f matrix4f = new Matrix4f();
        matrix4f.setIdentity();
        matrix4f.translate(pos);
        matrix4f.rotate((float) Math.toRadians(rot.x), new Vector3f(1, 0, 0));
        matrix4f.rotate((float) Math.toRadians(rot.y), new Vector3f(0, 1, 0));
        matrix4f.rotate((float) Math.toRadians(rot.z), new Vector3f(0, 0, 1));
        matrix4f.scale(new Vector3f(size, size, size));
        return matrix4f;
    }

    /**
     * Ajoute un addons
     *
     * @param entityAddons
     * @return
     */
    public Entity addAddon(EntityAddons entityAddons) {
        entityAddons.setEntity(this);
        addons.add(entityAddons);
        return this;
    }

    public <T extends EntityAddons> T getAddonsByClass(Class<T> addonsClass) {
        for (EntityAddons add : addons) {
            if (add.getClass().equals(addonsClass)) {
                return (T) add;
            }
        }
        return null;
    }

    /**
     * Détruit l'entité
     */
    public void destroy() {
        this.alive = false;
    }

    public boolean isDead() {
        return !isAlive();
    }
}
