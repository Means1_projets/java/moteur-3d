package fr.eisti.motor.opengl.entity.addons;

import fr.eisti.motor.opengl.entity.Entity;
import lombok.Getter;
import lombok.Setter;

public abstract class EntityAddons {

    @Setter
    @Getter
    private Entity entity;
}
