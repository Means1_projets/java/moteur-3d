package fr.eisti.motor.opengl.shader;

import org.lwjgl.util.vector.Vector3f;

public class ColoredShader extends RawModelShader {

    private static final String VERTEX_FILE = "res/shaders/colored/ColoredLightvertexShader.glsl";
    private static final String FRAGMENT_FILE = "res/shaders/colored/ColoredLightfragmentShader.glsl";


    private int material_ambient;
    private int material_diffuse;
    private int material_specular;

    public ColoredShader() {
        super(VERTEX_FILE, FRAGMENT_FILE);
    }

    /**
     * On récupère l'id des variable qui sont dans le shader
     */
    @Override
    protected void getAllUniformLocations() {
        super.getAllUniformLocations();


        material_ambient = super.getUniformLocation("material.ambient");
        material_diffuse = super.getUniformLocation("material.diffuse");
        material_specular = super.getUniformLocation("material.specular");

    }

    /**
     * Mise à jour des couleurs dans le shader grâce aux id des variables du shader
     *
     * @param ambient
     * @param diffuse
     * @param specular
     */
    public void loadColor(Vector3f ambient, Vector3f diffuse, Vector3f specular) {
        super.loadVector3f(material_ambient, ambient);
        super.loadVector3f(material_diffuse, diffuse);
        super.loadVector3f(material_specular, specular);
    }

}
