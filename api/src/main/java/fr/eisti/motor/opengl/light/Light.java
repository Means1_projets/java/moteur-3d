package fr.eisti.motor.opengl.light;

import fr.eisti.motor.opengl.loader.ColorADS;
import lombok.Getter;
import lombok.Setter;
import org.lwjgl.util.vector.Vector3f;

public class Light {

    @Getter
    @Setter
    private Vector3f pos = new Vector3f(0F, 0F, 0F), dir = new Vector3f(0F, 0F, 0F);
    @Getter
    @Setter
    private ColorADS color;
    @Getter
    @Setter
    private LightType lightType;
    @Getter
    @Setter
    private float constant = 1.0F;
    @Getter
    @Setter
    private float linear = 0.045F;
    @Getter
    @Setter
    private float quadratic = 0.0016F;
    @Getter
    @Setter
    private float cutOff = (float) Math.cos(Math.toRadians(17.5));
    @Getter
    private float outerCutOff = (float) Math.cos(Math.toRadians(12.5));

    /**
     * Constructeur appellé par Json
     */
    public Light() {

    }

    /**
     * Constructeur de Light
     *
     * @param pos         Vecto3f Position
     * @param color       ColorADS Ambient Diffuse Specular
     * @param dir         Vector3f Direction
     * @param lightType   LightType type de light
     * @param constant    float (attenuation)
     * @param linear      float (attenuation)
     * @param quadratic   float (attenuation)
     * @param cutOff      float Angle lampe
     * @param outerCutOff float Angle lampe
     */
    public Light(Vector3f pos, ColorADS color, Vector3f dir, LightType lightType, float constant, float linear, float quadratic, float cutOff, float outerCutOff) {
        this.pos = pos;
        this.color = color;
        this.dir = dir;
        this.lightType = lightType;
        this.constant = constant;
        this.linear = linear;
        this.cutOff = cutOff;
        this.outerCutOff = outerCutOff;
        this.quadratic = quadratic;
    }

    /**
     * Constructeur de Light
     *
     * @param pos       Vecto3f Position
     * @param color     ColorADS Ambient Diffuse Specular
     * @param dir       Vector3f Direction
     * @param lightType LightType type de light
     * @param constant  float (attenuation)
     * @param linear    float (attenuation)
     * @param quadratic float (attenuation)
     */
    public Light(Vector3f pos, ColorADS color, Vector3f dir, LightType lightType, float constant, float linear, float quadratic) {
        this.pos = pos;
        this.color = color;
        this.dir = dir;
        this.lightType = lightType;
        this.constant = constant;
        this.linear = linear;
        this.quadratic = quadratic;
    }


    /**
     * Constructeur de Light
     *
     * @param pos       Vecto3f Position
     * @param color     ColorADS Ambient Diffuse Specular
     * @param dir       Vector3f Direction
     * @param lightType LightType type de light
     */
    public Light(Vector3f pos, ColorADS color, Vector3f dir, LightType lightType) {
        this.pos = pos;
        this.color = color;
        this.dir = dir;
        this.lightType = lightType;
    }

    /**
     * Clone l'instance la light
     *
     * @return Light
     */
    public Light clone() {
        return new Light(new Vector3f(this.pos), new ColorADS(this.color.getAmbient(), this.color.getDiffuse(), this.color.getSpecular()), new Vector3f(this.dir), this.lightType, this.constant, this.linear, this.quadratic, this.cutOff, this.outerCutOff);
    }
}
