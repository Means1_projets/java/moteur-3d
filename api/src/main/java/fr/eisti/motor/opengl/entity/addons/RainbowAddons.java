package fr.eisti.motor.opengl.entity.addons;

import fr.eisti.motor.Motor;
import fr.eisti.motor.opengl.model.ColoredModel;
import fr.eisti.motor.opengl.shader.ColoredShader;
import fr.eisti.motor.opengl.shader.RawModelShader;
import org.lwjgl.util.vector.Vector3f;

import java.util.Random;

public class RainbowAddons extends EntityAddons implements ICommon {

    private final float a;
    private final float b;
    private final float c;
    private float i = 0;

    public RainbowAddons() {
        Random random = new Random();
        a = random.nextFloat();
        b = random.nextFloat() * 3;
        c = random.nextFloat() * 7;
    }

    /**
     * Envoie au shader une couleur random
     *
     * @param shader
     */
    @Override
    public void execute(RawModelShader shader) {
        if (shader instanceof ColoredShader && getEntity().getModel() instanceof ColoredModel) {
            Vector3f color = new Vector3f((float) Math.sin(i * a), (float) Math.sin(i * b), (float) Math.sin(i * c));
            ColoredModel model = (ColoredModel) getEntity().getModel();
            Motor.getInstance().getColoredShader().loadColor(color, color, model.getColor().getSpecular());
        }
        i += Math.PI / 500;
    }
}
