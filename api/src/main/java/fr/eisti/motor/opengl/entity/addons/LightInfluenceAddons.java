package fr.eisti.motor.opengl.entity.addons;

import fr.eisti.motor.opengl.shader.RawModelShader;

public class LightInfluenceAddons extends EntityAddons implements ICommon {

    private final boolean lightInfluence;


    public LightInfluenceAddons(boolean lightInfluence) {
        this.lightInfluence = lightInfluence;
    }

    @Override
    public void execute(RawModelShader shader) {
        shader.setLightInfluence(lightInfluence);
    }
}
