package fr.eisti.motor.opengl.entity.addons;

import fr.eisti.motor.opengl.light.Light;
import fr.eisti.motor.opengl.shader.RawModelShader;

public class LightEditAddons extends EntityAddons implements ILightExecute {

    Light light;


    public LightEditAddons(Light light) {
        this.light = light;
    }

    /**
     * Envoie au shader la bonne couleur de la light
     *
     * @param shader
     * @param i
     */
    @Override
    public void execute(RawModelShader shader, int i) {
        shader.loadLight(getEntity(), light, i);
    }
}
