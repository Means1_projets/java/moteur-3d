package fr.eisti.motor.opengl.entity.addons;

import fr.eisti.motor.Motor;
import fr.eisti.motor.opengl.model.ColoredModel;
import fr.eisti.motor.opengl.shader.ColoredShader;
import fr.eisti.motor.opengl.shader.RawModelShader;
import org.lwjgl.util.vector.Vector3f;

public class EditColorAddons extends EntityAddons implements ICommon {
    private Vector3f light;

    public EditColorAddons(Vector3f light) {
        this.light = light;
    }

    /**
     * Envoie au shader une couleur random
     *
     * @param shader
     */
    @Override
    public void execute(RawModelShader shader) {
        if (light != null && shader instanceof ColoredShader && getEntity().getModel() instanceof ColoredModel) {
            ColoredModel model = (ColoredModel) getEntity().getModel();
            Motor.getInstance().getColoredShader().loadColor(light, light, model.getColor().getSpecular());
        }
    }

    public Vector3f getLight() {
        return light;
    }

    public void setLight(Vector3f light) {
        this.light = light;
    }
}
