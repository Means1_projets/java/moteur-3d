package fr.eisti.motor.opengl.light;

import lombok.Getter;

public enum LightType {
    ALLDIR(0),
    DIRECTION(1),
    FLASHLIGHT(2);


    @Getter
    private final int id;

    LightType(int i) {

        id = i;
    }
}