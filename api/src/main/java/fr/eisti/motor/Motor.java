package fr.eisti.motor;

import fr.eisti.motor.opengl.engine.Camera;
import fr.eisti.motor.opengl.engine.DisplayManager;
import fr.eisti.motor.opengl.engine.Renderer;
import fr.eisti.motor.opengl.entity.Entity;
import fr.eisti.motor.opengl.light.Light;
import fr.eisti.motor.opengl.loader.Loader;
import fr.eisti.motor.opengl.shader.ColoredShader;
import fr.eisti.motor.opengl.shader.TexturedShader;
import lombok.Getter;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.vector.Vector3f;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.List;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL13.GL_MULTISAMPLE;

/**
 *
 */
public abstract class Motor {


    @Getter
    private static Motor instance;
    @Getter
    private static String path;

    static {
        //TODO a changer
        path = new File("").getAbsolutePath();
        int index;
        if ((index = path.indexOf("\\motor")) != -1) {
            path = path.substring(0, index) + "\\motor\\";
        } else {
            path = path.substring(0, path.indexOf("/motor")) + "/motor/";
        }
    }

    @Getter
    protected Loader loader;
    @Getter
    protected TexturedShader texturedShader;
    @Getter
    protected ColoredShader coloredShader;
    @Getter
    protected Camera camera;
    @Getter
    protected Renderer renderer;

    /**
     * Constructeur de la class Motor
     */
    protected Motor() {
        System.out.println("Motor3D : Starting...");
        try {
            System.out.println("Motor3D : Adding openGL Librairie...");
            addDir();
            System.out.println("Motor3D : Done");
            System.out.println("Motor3D : Initialisation");
            //On stock l'instance du moteur dans une variable static pour pouvoir avoir Motor.getInstance() qui sera cette instance du motor
            instance = this;
            //Création de la fenêtre
            DisplayManager.createDisplay();
            //Chargement des différents modules
            loader = new Loader();
            texturedShader = new TexturedShader();
            coloredShader = new ColoredShader();
            camera = new Camera();
            renderer = new Renderer();
            System.out.println("Motor3D : Lift off !");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void addDir() throws IOException {
        try {
            String s = new File("").getAbsolutePath();
            int index;
            if ((index = s.indexOf("\\motor")) != -1) {
                s = s.substring(0, index) + "\\motor\\libs";
            } else {
                s = s.substring(0, s.indexOf("/motor")) + "/motor/libs";
            }
            Field field = ClassLoader.class.getDeclaredField("usr_paths");
            field.setAccessible(true);
            String[] paths = (String[]) field.get(null);
            for (int i = 0; i < paths.length; i++) {
                if (s.equals(paths[i])) {
                    return;
                }
            }

            String[] tmp = new String[paths.length + 1];
            System.arraycopy(paths, 0, tmp, 0, paths.length);
            tmp[paths.length] = s;
            field.set(null, tmp);
            System.setProperty("java.library.path", System.getProperty("java.library.path") + File.pathSeparator + s);
        } catch (IllegalAccessException e) {
            throw new IOException("Failed to get permissions to set library path");
        } catch (NoSuchFieldException e) {
            throw new IOException("Failed to get field handle to set library path");
        }
    }

    /**
     * Fonction qui est contenue dans la scène, elle sera appellé au lancement de la scène
     */
    public abstract void setup();

    /**
     * Fonction qui est contenue dans la scène, elle sera appellé à chaque affichage
     */
    protected abstract void draw();

    /**
     * Lancement du moteur
     */
    public void start() {
        System.out.println("Motor3D : Start !");
        //On appelle setup
        setup();

        int fps = 0;
        long time = System.currentTimeMillis();


        //On active Buffer Z
        GL11.glEnable(GL11.GL_DEPTH_TEST);

        //On cache les faces qui ne sont pas visible
        GL11.glEnable(GL11.GL_CULL_FACE);
        GL11.glCullFace(GL11.GL_BACK);

        //Transparence
        GL11.glEnable(GL_BLEND);
        GL11.glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

        //AntiAliasing
        glEnable(GL_MULTISAMPLE);


        //Réglage sur les textures
        glEnable(GL_TEXTURE_2D);


        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

        //Boucle principale du moteur

        while (!Display.isCloseRequested()) {

            //Calcul des fps
            if (time < System.currentTimeMillis()) {
                time = System.currentTimeMillis() + 1000;
                Display.setTitle("Moteur 3D : " + fps);
                fps = 0;
            }
            GL11.glViewport(0, 0, Display.getWidth(), Display.getHeight());


            //On update la caméra (écoute des touches..)
            camera.update();
            //On appelle la fonction draw de la scène
            draw();
            //On calcul la scène
            renderer.render();
            //On affiche la scène
            DisplayManager.updateDisplay();
            Display.sync(60);
            fps++;

        }
        //Lors de l'arrêt du proggrma
        loader.cleanUp();
        DisplayManager.closeDisplay();
    }


    /**
     * Fonction disponible dans la class de la scène pour charger une entité
     *
     * @param name String nom du model
     * @param i    float position x
     * @param i1   float position y
     * @param i2   float position z
     * @param i3   float direction x
     * @param i4   float direction y
     * @param i5   float direction z
     * @param v    float taille
     * @return Entity
     */
    public Entity loadEntity(String name, float i, float i1, float i2, float i3, float i4, float i5, float v) {
        return loader.loadEntity(name, i, i1, i2, i3, i4, i5, v);
    }


    /**
     * Pour changer la couleur du background
     *
     * @param vector3f
     */
    public void setBackgroundColor(Vector3f vector3f) {
        renderer.setColorFond(vector3f);
    }

    /**
     * Pour changer le point de spawn
     *
     * @param vector3f
     */
    public void setSpawnPosition(Vector3f vector3f) {
        camera.setPos(vector3f);
    }

    /**
     * Pour enlever toutes les lights qui ne sont pas lié à une entité
     */
    public void clearLigths() {
        renderer.getLights().clear();
    }

    /**
     * Pour rajouter une light
     *
     * @param light Light
     */
    public void addLight(Light light) {
        renderer.getLights().add(light);
    }

    /**
     * Pour enlever une light
     *
     * @param light Light
     */
    public void removeLight(Light light) {
        renderer.getLights().remove(light);
    }

    /**
     * Pour rajouter une list de light
     *
     * @param light ArrayList<Light>
     */
    public void addAllLights(List<Light> light) {
        renderer.getLights().addAll(light);
    }

    /**
     * Pour récuperer la position du joueur
     *
     * @return Vector3f
     */
    public Vector3f getUserPos() {
        return camera.getPos();
    }

    /**
     * Pour récuperer le vecteur direction de la caméra
     *
     * @return Vector3f
     */
    public Vector3f getUserDir() {
        return camera.getDir();
    }
}
