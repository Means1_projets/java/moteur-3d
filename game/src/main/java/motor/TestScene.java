package motor;

import fr.eisti.motor.Motor;
import fr.eisti.motor.opengl.entity.Entity;
import fr.eisti.motor.opengl.entity.addons.LifeSpan;
import fr.eisti.motor.opengl.entity.addons.LightEditAddons;
import fr.eisti.motor.opengl.entity.addons.RainbowAddons;
import fr.eisti.motor.opengl.entity.addons.RainbowLightAddons;
import fr.eisti.motor.opengl.light.Light;
import fr.eisti.motor.opengl.light.LightType;
import fr.eisti.motor.opengl.loader.ColorADS;
import org.lwjgl.util.vector.Vector3f;

/**
 * Class principale
 *
 * @version 1.0
 */
public class TestScene extends Motor {

    private Entity dragon;
    private Entity sol, sol2;
    private Entity cube;
    private Entity lapin;
    private Entity teapot;
    private Entity cerf;
    private Entity flashlight, flashlight2;

    private float i = 0;
    private float alpla = 10F, beta = 8F / 3F, p = 24.3F, presi = 0.001F;
    private Vector3f pos = new Vector3f(10F, 10F, 20F);

    /**
     * Fonction principale du programme, elle permet de lancer la scène.
     * Elle est appellé au lancement du programme
     *
     * @param args unused
     */
    public static void main(String[] args) {
        TestScene testScene = new TestScene();
        testScene.start();
    }

    /**
     * Fonction setup, lancé lors du démarrage (ou reload)
     */
    @Override
    public void setup() {

        setSpawnPosition(new Vector3f(0, 10, 0));

        setBackgroundColor(new Vector3f(0.8F, 0.6F, 0.9F));

        //Définition des models
        //loadEntity("sol", -10,0,-10,0,0,0,10f);
        lapin = loadEntity("lapin", 25, 10, 10, 0, 0, 0, 10f);
        dragon = loadEntity("dragon", 10, 10, 0, 0, 0, 0, 0.1f);
        dragon.addAddon(new RainbowAddons());
        sol = loadEntity("sun", 10, 5, 10, 0, 0, 0, 0.00001f);
        sol2 = loadEntity("sun", 10, 5, 10, 0, 0, 0, 0.00001f);
        cube = loadEntity("cube", 0, 5, 0, 0, 0, 0, 1);
        teapot = loadEntity("teapot", 5, 5, -5, 0, 0, 0, 0.5F);
        cerf = loadEntity("deer", 5, 15, -5, 0, 0, 0, 0.01F);
        loadEntity("stall", 5, 0, 20, 0, 0, 0, 1F);
        loadEntity("test", -10, 0, 20, 0, 0, 0, 1F);
        loadEntity("cube", 6, 5, 0, 0, 0, 0, 1);
        flashlight = loadEntity("flashlight", 3, 2.5F, 20, -90, 0, 0, 0.1F);

        //Récuparation de la light de la flashlight

        Light light = flashlight.getModel().getLight().clone();

        //Définition d'une nouvelle couleur

        light.setColor(new ColorADS(new Vector3f(0F, 0F, 0F), new Vector3f(0F, 0F, 0.5F), new Vector3f(0F, 0F, 1F)));

        //Appliquer la nouvelle couleur de la light sur l'entité flashlight

        flashlight.addAddon(new LightEditAddons(light));

        flashlight2 = loadEntity("flashlight", 8, 2.5F, 20, -90, 0, 0, 0.1F);

        //Mise en place de flashlight qui ont des couleurs qui varie avec le temps (avec le RainbowLightAddons)

        loadEntity("flashlight", 25, 15, 10, 180F, 0, 0, 0.1F).addAddon(new RainbowLightAddons());
        loadEntity("flashlight", 25, 5, 10, 0F, 0, 0, 0.1F).addAddon(new RainbowLightAddons());
        loadEntity("flashlight", 25, 11, 5, 90F, 90F, 0F, 0.1F).addAddon(new RainbowLightAddons());
        loadEntity("flashlight", 25, 11, 15, -90F, 90F, 0F, 0.1F).addAddon(new RainbowLightAddons());
        loadEntity("flashlight", 30, 11, 10, 90F, 0F, 90F, 0.1F).addAddon(new RainbowLightAddons());
        loadEntity("flashlight", 20, 11, 10, 90F, 0F, -90F, 0.1F).addAddon(new RainbowLightAddons());
    }

    /**
     * Calcul équation diff
     *
     * @param a Vector3f
     * @return Vector3f
     */
    private Vector3f eval(Vector3f a) {
        Vector3f d = new Vector3f();
        d.x = alpla * (a.y - a.x);
        d.y = p * a.x - a.y - a.x * a.z;
        d.z = a.x * a.y - beta * a.z;
        return d;
    }

    /**
     * Calcul équation diff
     *
     * @param yi Vector3f
     * @return Vector3f
     */
    private Vector3f nextf(Vector3f yi) {
        Vector3f a = eval(yi);
        a.x = yi.x + presi * a.x;
        a.y = yi.y + presi * a.y;
        a.z = yi.z + presi * a.z;
        return a;
    }

    /**
     * Fonction draw, lancé lors de l'affichage
     */
    @Override
    public void draw() {
        clearLigths();
        addLight(new Light(new Vector3f(getUserPos()), new ColorADS(new Vector3f(0, 0, 0), new Vector3f(0.5F, 0.5F, 0), new Vector3f(0, 0, 0)), new Vector3f(getUserDir()), LightType.FLASHLIGHT));
        //Rotation et changement de position des entités
        dragon.increaseRotation(1F, 0, 0);
        lapin.increaseRotation(0F, 1F, 0);
        sol.increaseRotation(1F, -3F, 5F);
        sol.setPos(new Vector3f(3F * ((float) Math.cos(i)), 5, 3F * ((float) Math.sin(i))));
        sol2.setPos(new Vector3f(6 + 3F * ((float) Math.cos(-i)), 5, 3F * ((float) Math.sin(-i))));
        i += Math.PI / 300;
        cube.increaseRotation(0.1F, -0.3F, 0.2F);
        teapot.increaseRotation(0.1f, -0.2F, 5f);
        cerf.increaseRotation(0.1f, -0.2F, 0.1f);
        flashlight.increaseRotation(1f, 5f, -0.1f);
        flashlight2.increaseRotation(0.5f, -1.32f, 1f);

        for (int j = 0; j < 10; j++)
            pos = nextf(pos);
        loadEntity("triangle", pos.x / 10F + 10F, pos.y / 10F + 10F, pos.z / 10F, 0F, 0f, 0f, 0.01F).addAddon(new LifeSpan(1000));


    }
}