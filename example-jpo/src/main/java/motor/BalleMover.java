package motor;

import fr.eisti.motor.Motor;
import fr.eisti.motor.opengl.entity.Entity;
import fr.eisti.motor.opengl.entity.addons.LightEditAddons;
import fr.eisti.motor.opengl.light.Light;
import fr.eisti.motor.opengl.light.LightType;
import fr.eisti.motor.opengl.loader.ColorADS;
import org.lwjgl.util.vector.Vector3f;

public class BalleMover {

    private Entity entity;
    private double mass;
    private Vector3f acceleration, velocity;


    public BalleMover(Motor motor, float x, float y, float z, float masse) {
        this.entity = motor.loadEntity("sun", x, y, z, 0, 0, 0, 0.00001f * masse)
                .addAddon(new LightEditAddons(
                        new Light(new Vector3f(), new ColorADS(new Vector3f(0.1F, 0.1F, 0.1F), new Vector3f(0.2F, 0.2F, 0.2F), new Vector3f(0.2F, 0.2F, 0.2F)), new Vector3f(), LightType.ALLDIR)
                ));
        acceleration = new Vector3f();
        this.mass = masse;
        velocity = new Vector3f(0F, 0, 0);
    }

    public void applyForce(Vector3f vector) {
        vector.scale(1F / (float) mass);
        acceleration.translate(vector.x, vector.y, vector.z);
    }

    public void update() {
        velocity.translate(acceleration.x, acceleration.y, acceleration.z);


        entity.increasePosition(velocity.x, velocity.y, velocity.z);

        acceleration = new Vector3f();
    }

    public void checkEdge(Entity terrain) {
        if (entity.getPos().getX() > terrain.getSize() * 10 - 0.25F) {
            velocity.setX(-velocity.getX());
            entity.getPos().setX((terrain.getSize() * 10 - 0.25F) * 2 - entity.getPos().getX());
        }
        if (entity.getPos().getX() < 0.25F) {
            entity.getPos().setX(0.50F - entity.getPos().getX());
            velocity.setX(-velocity.getX());
        }
        if (entity.getPos().getZ() > terrain.getSize() * 10 - 0.25F) {
            entity.getPos().setZ((terrain.getSize() * 10 - 0.25F) * 2 - entity.getPos().getZ());
            velocity.setZ(-velocity.getZ());
        }
        if (entity.getPos().getZ() < 0.25F) {
            entity.getPos().setZ(0.50F - entity.getPos().getZ());
            velocity.setZ(-velocity.getZ());
        }
        if (entity.getPos().getY() < 0.25F) {
            velocity.setY(-velocity.getY());
            entity.getPos().setY(0.50F - entity.getPos().getY());
        }
    }

}
