package motor;

import fr.eisti.motor.Motor;
import fr.eisti.motor.opengl.entity.Entity;
import fr.eisti.motor.opengl.entity.addons.RainbowAddons;
import org.lwjgl.util.vector.Vector3f;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class Mover {

    static Random random = new Random();
    static Map<String, Float> types = new HashMap<>();

    static {
        types.put("cube", 0.5F);
        types.put("teapot", 0.2F);
        types.put("lapin", 10F);
        types.put("dragon", 0.1F);
    }

    private Entity entity;
    private double mass;
    private Vector3f acceleration, velocity;
    private float a, b, c;
    private int randomPick = random.nextInt(types.size());

    public Mover(Motor motor) throws Exception {
        this.entity = motor.loadEntity(getEntry().getKey(), random.nextFloat() * 20 - 10, random.nextFloat() * 20 - 10, random.nextFloat() * 20 - 10, 0, 0, 0, getEntry().getValue());
        this.mass = random.nextInt(10) + 1;
        entity.addAddon(new RainbowAddons());
        acceleration = new Vector3f();
        velocity = new Vector3f(0.1F, 0, 0);
        Random random = new Random();
        a = random.nextFloat();
        b = random.nextFloat() * 3;
        c = random.nextFloat() * 7;
    }

    public Mover(Entity entity, double mass) {
        entity.addAddon(new RainbowAddons());
        this.mass = mass;
        this.entity = entity;
        acceleration = new Vector3f();
        velocity = new Vector3f();
        Random random = new Random();
        a = random.nextFloat();
        b = random.nextFloat() * 3;
        c = random.nextFloat() * 7;
    }

    private Map.Entry<String, Float> getEntry() throws Exception {
        int i = 0;
        for (Map.Entry<String, Float> entry : types.entrySet()) {
            if (i == randomPick || i == types.size() - 1) {
                return entry;
            }
            i++;
        }
        throw new Exception("impossible");
    }

    public Vector3f attract(Mover entity) {
        Vector3f force = Vector3f.sub(this.entity.getPos(), entity.entity.getPos(), null);
        double distance = force.length();
        distance = Math.min(Math.max(distance, 25), 5);
        if (distance == 0) {
            distance = 0.1;
            force.set(force.x / 0.1f, force.y / 0.1F, force.z / 0.1F);
        } else
            force.normalise();
        double strength = (0.00001F * mass * entity.mass) / (distance * distance);
        force.scale((float) strength);
        return force;

    }

    public void applyForce(Vector3f vector) {
        vector.scale((float) mass);
        acceleration.translate(vector.x, vector.y, vector.z);
    }

    public void update() {
        velocity.translate(acceleration.x, acceleration.y, acceleration.z);
        entity.increasePosition(velocity.x, velocity.y, velocity.z);
        acceleration = new Vector3f();
        entity.increaseRotation(a, b, c);
    }

    public Entity getEntity() {
        return entity;
    }
}
