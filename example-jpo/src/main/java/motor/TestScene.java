package motor;

import fr.eisti.motor.Motor;
import fr.eisti.motor.opengl.entity.Entity;
import fr.eisti.motor.opengl.entity.addons.LifeSpan;
import fr.eisti.motor.opengl.entity.addons.LightEditAddons;
import fr.eisti.motor.opengl.entity.addons.LightInfluenceAddons;
import fr.eisti.motor.opengl.entity.addons.RainbowLightAddons;
import fr.eisti.motor.opengl.light.Light;
import fr.eisti.motor.opengl.light.LightType;
import fr.eisti.motor.opengl.loader.ColorADS;
import org.lwjgl.input.Keyboard;
import org.lwjgl.util.vector.Vector3f;

import java.util.ArrayList;

/**
 * Class principale
 *
 * @version 1.0
 */
public class TestScene extends Motor {

    Mover[] entities;
    BalleMover[] balles;
    private int step = 0;
    private boolean press = false;
    private boolean update = true;
    private Entity entity;
    private float i = 0;
    private boolean[] dir = new boolean[4];
    private boolean press2 = false;
    private boolean updateDir = false;
    private ArrayList<Entity> entitys = new ArrayList<>();
    private FigureMath fig1 = new FigureMath((x, z) -> (float) ((x * x * z * z) / Math.sqrt(x * x + z * z)), -1, 1, -1, 1, 50, 0, 0, 0);
    private FigureMath fig2 = new FigureMath((x, z) -> (float) Math.cos(x * x + z * z) / (x * x + z * z) * 10, 1, (float) Math.PI, 1, (float) Math.PI, 100, 4, 0, -1);
    private FigureMath fig3 = new FigureMath((x, z) -> (float) Math.tan(x * x + z * z) / (x * x + z * z) / 10F, -(float) Math.PI / 2, (float) Math.PI / 2, -(float) Math.PI / 2, (float) Math.PI / 2, 60, 0, 0, 10);
    private FigureMath fig4 = new FigureMath((x, z) -> (float) Math.sin(x * x + z * z * 2) / (x * x * 9 + z * z * 9) * 3F, -1, 1, -1, 1, 60, -5, 0, 0);
    private FigureMath fig5 = new FigureMath((x, z) -> x * x, -1, 1, 0, 0, 60, 5, 0, 5);
    private FigureMath seleted;

    private float alpla = 10F, beta = 5F / 3F, p = 24.5F, presi = 0.001F;
    private Vector3f pos = new Vector3f(10F, 10F, 20F);

    /**
     * Fonction principale du programme, elle permet de lancer la scène.
     * Elle est appellé au lancement du programme
     *
     * @param args unused
     */
    public static void main(String[] args) {
        TestScene testScene = new TestScene();
        testScene.start();
    }

    /**
     * Calcul équation diff
     *
     * @param a Vector3f
     * @return Vector3f
     */
    private Vector3f eval(Vector3f a) {
        Vector3f d = new Vector3f();
        d.x = alpla * (a.y - a.x);
        d.y = p * a.x - a.y - a.x * a.z;
        d.z = a.x * a.y - beta * a.z;
        return d;
    }

    /**
     * Calcul équation diff
     *
     * @param yi Vector3f
     * @return Vector3f
     */
    private Vector3f nextf(Vector3f yi) {
        Vector3f a = eval(yi);
        a.x = yi.x + presi * a.x;
        a.y = yi.y + presi * a.y;
        a.z = yi.z + presi * a.z;
        return a;
    }

    /**
     * Fonction setup, lancé lors du démarrage (ou reload)
     */
    @Override
    public void setup() {

        setSpawnPosition(new Vector3f(0, 10, 0));
        setBackgroundColor(new Vector3f(0.8F, 0.8F, 0.8F));


    }

    /**
     * Fonction draw, lancé lors de l'affichage
     */
    @Override
    public void draw() {
        if (!press && Keyboard.isKeyDown(Keyboard.KEY_RIGHT)) {
            press = true;
            update = true;
            step++;
        } else if (!press && Keyboard.isKeyDown(Keyboard.KEY_LEFT)) {
            press = true;
            update = true;
            step--;
        } else if (press && (!Keyboard.isKeyDown(Keyboard.KEY_RIGHT) && !Keyboard.isKeyDown(Keyboard.KEY_LEFT))) {
            press = false;
        }

        if (update) {
            update = false;
            getRenderer().getEntitys().values().forEach(Entity::destroy);
            getRenderer().getEntitys().clear();
            entitys.forEach(Entity::destroy);
            entitys.clear();
            clearLigths();

            switch (step) {
                case 0:
                    loadEntity("lapin", 0, 0, 0, 0, 0, 0, 10f).addAddon(new LightInfluenceAddons(false));
                    break;
                case 1:
                    entity = loadEntity("lapin", 0, 0, 0, 0, 0, 0, 10f).addAddon(new LightInfluenceAddons(false));
                    break;
                case 2:
                    loadEntity("lapin", 0, 0, 0, 0, 0, 0, 10f);
                    loadEntity("arrow", -2, 3, 0, 90, 45, 0, 0.5f);
                    loadEntity("arrow", -3F, 3F, 0, 90, 45, 0, 0.5f);
                    loadEntity("arrow", -1F, 3F, 0, 90, 45, 0, 0.5f);

                    addLight(new Light(new Vector3f(), new ColorADS(new Vector3f(), new Vector3f(0.5F, 0.5F, 0.5F), new Vector3f()), new Vector3f(0.5F, -1, 0), LightType.DIRECTION));
                    break;
                case 3:
                    loadEntity("lapin", 0, 0, 0, 0, 0, 0, 10f);
                    loadEntity("sun", 3F * ((float) Math.cos(i)), 1, 3F * ((float) Math.sin(i)), 0, 0, 0, 0.00001f);
                    break;
                case 4:
                    loadEntity("lapin", 0, 0, 0, 0, 0, 0, 10f);
                    entity = loadEntity("sun", 3F * ((float) Math.cos(i)), 1, 3F * ((float) Math.sin(i)), 0, 0, 0, 0.00001f);
                    break;
                case 5:
                    loadEntity("lapin", 0, 0, 0, 0, 0, 0, 10f);
                    loadEntity("flashlight", 0, 1, 5, -90F, 0, 0, 0.1F);
                    break;
                case 14:
                    entity = loadEntity("dragon", 0, 0.5F, 0, 0, 0, 0, 0.1f);
                case 13:
                case 12:
                    loadEntity("flashlight", -3, 1, 3, -90F, 0, -45, 0.1F).addAddon(new RainbowLightAddons());
                    loadEntity("flashlight", 3, 1, -3, 90F, 0, 45, 0.1F).addAddon(new RainbowLightAddons());
                    loadEntity("flashlight", 3, 1, 3, -90F, 0, 45, 0.1F).addAddon(new RainbowLightAddons());
                    loadEntity("flashlight", -3, 1, -3, -90F, 0, -135, 0.1F).addAddon(new RainbowLightAddons());
                    loadEntity("flashlight", -2.25F, 3.75F, -2.25F, 0, -45, -135, 0.1F).addAddon(new RainbowLightAddons());
                    loadEntity("flashlight", 2.25F, 3.75F, -2.25F, 105, 38, 45, 0.1F).addAddon(new RainbowLightAddons());
                    loadEntity("flashlight", 2.25F, 3.75F, 2.25F, 0, -40, 130, 0.1F).addAddon(new RainbowLightAddons());
                    loadEntity("flashlight", -2.25F, 3.75F, 2.25F, 47, -13, -148, 0.1F).addAddon(new RainbowLightAddons());
                    loadEntity("flashlight", -2.25F, -1.75F, 2.25F, -39, 0, -31, 0.1F).addAddon(new RainbowLightAddons());
                    loadEntity("flashlight", 2.25F, -1.75F, 2.25F, -43, 0, 32, 0.1F).addAddon(new RainbowLightAddons());
                    loadEntity("flashlight", -2.25F, -1.75F, -2.25F, 43, 0, -33, 0.1F).addAddon(new RainbowLightAddons());
                    loadEntity("flashlight", 2.25F, -1.75F, -2.25F, 41, 0, 34, 0.1F).addAddon(new RainbowLightAddons());
                    loadEntity("flashlight", 0F, -2F, -3.25F, 49, 0, 0, 0.1F).addAddon(new RainbowLightAddons());
                    loadEntity("flashlight", 0F, -2F, 3.25F, -49, 0, 0, 0.1F).addAddon(new RainbowLightAddons());
                    loadEntity("flashlight", 3.25F, -2F, 0, 0, 0, 51, 0.1F).addAddon(new RainbowLightAddons());
                    loadEntity("flashlight", -3.25F, -2F, 0, 0, 0, -51, 0.1F).addAddon(new RainbowLightAddons());
                    loadEntity("flashlight", 0F, 4F, -3.25F, 131, 0, 0, 0.1F).addAddon(new RainbowLightAddons());
                    loadEntity("flashlight", 0F, 4F, 3.25F, -131, 0, 0, 0.1F).addAddon(new RainbowLightAddons());
                    loadEntity("flashlight", 3.25F, 4F, 0, 0, 0, 131, 0.1F).addAddon(new RainbowLightAddons());
                    loadEntity("flashlight", -3.25F, 4F, 0, 0, 0, -131, 0.1F).addAddon(new RainbowLightAddons());
                case 11:
                    loadEntity("flashlight", 0, -3, 0, 0F, 0, 0, 0.1F).addAddon(new RainbowLightAddons());
                case 10:
                    loadEntity("flashlight", 0, 5, 0, 180, 0, 0, 0.1F).addAddon(new RainbowLightAddons());
                case 9:
                    loadEntity("flashlight", -5, 1, 0, -90F, 0, -90, 0.1F).addAddon(new RainbowLightAddons());
                case 8:
                    loadEntity("flashlight", 5, 1, 0, -90F, 0, 90, 0.1F).addAddon(new RainbowLightAddons());
                case 7:
                    loadEntity("flashlight", 0, 1, -5, 90F, 0, 0, 0.1F).addAddon(new RainbowLightAddons());
                case 6:
                    if (step < 14)
                        entity = loadEntity("lapin", 0, 0, 0, 0, 0, 0, 10f);
                    loadEntity("flashlight", 0, 1, 5, -90F, 0, 0, 0.1F).addAddon(new RainbowLightAddons());
                    break;
                case 15:
                    loadEntity("cube", 0, 0, 0, 0, 0, 0, 1);
                    loadEntity("sun", 3, 1, 3, 0, 0, 0, 0.00001f).addAddon(new LightEditAddons(new Light(new Vector3f(), new ColorADS(new Vector3f(0.5F, 0.5F, 0.5F), new Vector3f(1, 1, 1), new Vector3f(1F, 1F, 1F)), new Vector3f(), LightType.ALLDIR)));
                    break;
                case 16:
                    loadEntity("stall", 0, 0, 0, 0, 180, 0, 1F);
                    loadEntity("sun", 0, 5, 6, 0, 0, 0, 0.00001f).addAddon(new LightEditAddons(new Light(new Vector3f(), new ColorADS(new Vector3f(0.1F, 0.1F, 0.1F), new Vector3f(1, 1, 1), new Vector3f(1F, 1F, 1F)), new Vector3f(), LightType.ALLDIR)));
                    break;
                case 17:
                    loadEntity("test", 0, 0, 0, 0, 0, 0, 1F);
                    loadEntity("sun", 0, 5, 6, 0, 0, 0, 0.00001f).addAddon(new LightEditAddons(new Light(new Vector3f(), new ColorADS(new Vector3f(0.1F, 0.1F, 0.1F), new Vector3f(1, 1, 1), new Vector3f(1F, 1F, 1F)), new Vector3f(), LightType.ALLDIR)));
                    break;
                case 18:
                    loadEntity("eisti", 0, 0, 0, 0, 0, 0, 1F);
                    loadEntity("sun", 5, 8, 7, 0, 0, 0, 0.00001f).addAddon(new LightEditAddons(new Light(new Vector3f(), new ColorADS(new Vector3f(0.1F, 0.1F, 0.1F), new Vector3f(1, 1, 1), new Vector3f(1F, 1F, 1F)), new Vector3f(), LightType.ALLDIR)));
                    break;
                case 19:
                    loadEntity("eistitroll", 0, 0, 0, 0, 0, 0, 1F);
                    loadEntity("sun", 5, 8, 7, 0, 0, 0, 0.00001f).addAddon(new LightEditAddons(new Light(new Vector3f(), new ColorADS(new Vector3f(0.1F, 0.1F, 0.1F), new Vector3f(1, 1, 1), new Vector3f(1F, 1F, 1F)), new Vector3f(), LightType.ALLDIR)));
                    loadEntity("deer", 5, 3, -15, 0, 0, 0, 0.01F);
                    break;
                case 20:
                    fig1.draw();
                    fig2.draw();
                    fig3.draw();
                    fig4.draw();
                    fig5.draw();
                    break;
                case 21:
                    balles = new BalleMover[]{new BalleMover(this, 2, 4, 2, 1)};
                    entity = loadEntity("terrain", 0, 0, 0, 0, 0, 0, 1f);
                    break;
                case 22:
                    balles = new BalleMover[]{new BalleMover(this, 2, 4, 2, 1),
                            new BalleMover(this, 3, 4, 2, 1F),
                            new BalleMover(this, 4, 4, 2, 1F),
                            new BalleMover(this, 5, 4, 2, 1F),
                            new BalleMover(this, 6, 4, 2, 1F)};
                    entity = loadEntity("terrain", 0, 0, 0, 0, 0, 0, 1f);
                    break;
                case 23:
                    entities = new Mover[50];
                    entities[0] = new Mover(loadEntity("sun", 0, 0, 0, 0, 0, 0, 0.00009f), 100);
                    try {

                        for (int j = 1; j < 50; j++) {
                            entities[j] = new Mover(this);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case 24:
                    loadEntity("cube", 0, 0, 0, 0, 0, 0, 1).addAddon(new LightInfluenceAddons(false));
                    loadEntity("cube", 0, 2, 0, 0, 0, 0, 1).addAddon(new LightInfluenceAddons(false));
                    loadEntity("cube", 0, 4, 0, 0, 0, 0, 1).addAddon(new LightInfluenceAddons(false));
                    loadEntity("cube", 0, 6, 0, 0, 0, 0, 1).addAddon(new LightInfluenceAddons(false));
                    loadEntity("cube", 0, 8, 0, 0, 0, 0, 1).addAddon(new LightInfluenceAddons(false));
                    loadEntity("cube", 2, 8, 0, 0, 0, 0, 1).addAddon(new LightInfluenceAddons(false));
                    loadEntity("cube", 4, 8, 0, 0, 0, 0, 1).addAddon(new LightInfluenceAddons(false));
                    loadEntity("cube", 2, 4, 0, 0, 0, 0, 1).addAddon(new LightInfluenceAddons(false));
                    loadEntity("cube", 14, 0, 0, 0, 0, 0, 1).addAddon(new LightInfluenceAddons(false));
                    loadEntity("cube", 14, 2, 0, 0, 0, 0, 1).addAddon(new LightInfluenceAddons(false));
                    loadEntity("cube", 14, 4, 0, 0, 0, 0, 1).addAddon(new LightInfluenceAddons(false));
                    loadEntity("cube", 14, 6, 0, 0, 0, 0, 1).addAddon(new LightInfluenceAddons(false));
                    loadEntity("cube", 14, 8, 0, 0, 0, 0, 1).addAddon(new LightInfluenceAddons(false));
                    loadEntity("cube", 12, 8, 0, 0, 0, 0, 1).addAddon(new LightInfluenceAddons(false));
                    loadEntity("cube", 16, 8, 0, 0, 0, 0, 1).addAddon(new LightInfluenceAddons(false));
                    loadEntity("cube", 12, 0, 0, 0, 0, 0, 1).addAddon(new LightInfluenceAddons(false));
                    loadEntity("cube", 16, 0, 0, 0, 0, 0, 1).addAddon(new LightInfluenceAddons(false));
                    loadEntity("cube", 24, 0, 0, 0, 0, 0, 1).addAddon(new LightInfluenceAddons(false));
                    loadEntity("cube", 24, 2, 0, 0, 0, 0, 1).addAddon(new LightInfluenceAddons(false));
                    loadEntity("cube", 24, 4, 0, 0, 0, 0, 1).addAddon(new LightInfluenceAddons(false));
                    loadEntity("cube", 24, 6, 0, 0, 0, 0, 1).addAddon(new LightInfluenceAddons(false));
                    loadEntity("cube", 24, 8, 0, 0, 0, 0, 1).addAddon(new LightInfluenceAddons(false));
                    loadEntity("cube", 26, 6, 0, 0, 0, 0, 1).addAddon(new LightInfluenceAddons(false));
                    loadEntity("cube", 28, 4, 0, 0, 0, 0, 1).addAddon(new LightInfluenceAddons(false));
                    loadEntity("cube", 30, 2, 0, 0, 0, 0, 1).addAddon(new LightInfluenceAddons(false));
                    loadEntity("cube", 32, 0, 0, 0, 0, 0, 1).addAddon(new LightInfluenceAddons(false));
                    loadEntity("cube", 32, 2, 0, 0, 0, 0, 1).addAddon(new LightInfluenceAddons(false));
                    loadEntity("cube", 32, 4, 0, 0, 0, 0, 1).addAddon(new LightInfluenceAddons(false));
                    loadEntity("cube", 32, 6, 0, 0, 0, 0, 1).addAddon(new LightInfluenceAddons(false));
                    loadEntity("cube", 32, 8, 0, 0, 0, 0, 1).addAddon(new LightInfluenceAddons(false));
                    loadEntity("dragon", 2, 5F, 0, 0, 0, 0, 0.17F);
                    loadEntity("deer", 2, 9, 0, 0, 0, 0, 0.005F);
                    loadEntity("cytech", 0, -4, -20, 0, 0, 0, 2).addAddon(new LightInfluenceAddons(false));
                    loadEntity("teapot", 14, 9, 0, 0, 0, 0, 0.5F);
                    loadEntity("lapin", 12, 0.7F, 0, 0, 0, 0, 10F);
                    loadEntity("bull", 29.5F, 6.5F, 0, 90, 230, 90, 0.6F);
                    addLight(new Light(new Vector3f(0, 10, 10), new ColorADS(new Vector3f(0, 0, 0.2F), new Vector3f(0.7F, 0, 1F), new Vector3f(0.7F, 0, 1)), new Vector3f(), LightType.ALLDIR));
                    addLight(new Light(new Vector3f(14, 10, 10), new ColorADS(new Vector3f(0, 0.09F, 0.2F), new Vector3f(0, 0.7F, 1F), new Vector3f(0, 0.7F, 1F)), new Vector3f(), LightType.ALLDIR));
                    addLight(new Light(new Vector3f(32, 10, 10), new ColorADS(new Vector3f(0.09F, 0, 0.2F), new Vector3f(0, 0, 1F), new Vector3f(0, 0, 1F)), new Vector3f(), LightType.ALLDIR));
                    break;
                default:
                    if (step > 24) {
                        step = 0;
                        update = true;
                    } else {
                        step = 24;
                        update = true;
                    }
            }
        }
        switch (step) {
            case 1:
                entity.increaseRotation(0.2F, 0.66F, -0.01F);
                break;
            case 4:
                if (entity != null) {
                    entity.setPos(new Vector3f(3F * ((float) Math.cos(i)), 1, 3F * ((float) Math.sin(i))));
                    i += Math.PI / 300;
                }
                break;
            case 13:
            case 14:
                entity.increaseRotation(0, 2F, 0);
                break;
            case 20:
                if (Keyboard.isKeyDown(Keyboard.KEY_1)) {
                    seleted = fig1;
                } else if (Keyboard.isKeyDown(Keyboard.KEY_2)) {
                    seleted = fig2;
                } else if (Keyboard.isKeyDown(Keyboard.KEY_3)) {
                    seleted = fig3;
                } else if (Keyboard.isKeyDown(Keyboard.KEY_4)) {
                    seleted = fig4;
                } else if (Keyboard.isKeyDown(Keyboard.KEY_5)) {
                    seleted = fig5;
                }
                if (Keyboard.isKeyDown(Keyboard.KEY_SUBTRACT) && seleted.getN() > 0) {
                    seleted.setN(seleted.getN() - 1);
                    seleted.draw();
                }
                if (Keyboard.isKeyDown(Keyboard.KEY_ADD)) {
                    seleted.setN(seleted.getN() + 1);
                    seleted.draw();
                }
                if (Keyboard.isKeyDown(Keyboard.KEY_NUMPAD8)) {
                    seleted.setXmin(seleted.getXmin() + 0.01F);
                    seleted.setXmax(seleted.getXmax() + 0.01F);
                    seleted.setXTranslation(seleted.getXTranslation() - 0.01F);
                    seleted.draw();
                }
                if (Keyboard.isKeyDown(Keyboard.KEY_NUMPAD4)) {
                    seleted.setZmin(seleted.getZmin() + 0.01F);
                    seleted.setZmax(seleted.getZmax() + 0.01F);
                    seleted.setZTranslation(seleted.getZTranslation() - 0.01F);
                    seleted.draw();
                }
                if (Keyboard.isKeyDown(Keyboard.KEY_NUMPAD6)) {
                    seleted.setZmin(seleted.getZmin() - 0.01F);
                    seleted.setZmax(seleted.getZmax() - 0.01F);
                    seleted.setZTranslation(seleted.getZTranslation() + 0.01F);
                    seleted.draw();
                }
                if (Keyboard.isKeyDown(Keyboard.KEY_NUMPAD2)) {
                    seleted.setXmin(seleted.getXmin() - 0.01F);
                    seleted.setXmax(seleted.getXmax() - 0.01F);
                    seleted.setXTranslation(seleted.getXTranslation() + 0.01F);
                    seleted.draw();
                }
                for (int j = 0; j < 10; j++)
                    pos = nextf(pos);
                loadEntity("triangle", pos.x / 10F, pos.y / 10F, pos.z / 10F - 10F, 0F, 0f, 0f, 0.01F).addAddon(new LifeSpan(1000));
                break;
            case 21:
            case 22:
                if (press2 && !Keyboard.isKeyDown(Keyboard.KEY_NUMPAD8) && !Keyboard.isKeyDown(Keyboard.KEY_NUMPAD6) &&
                        !Keyboard.isKeyDown(Keyboard.KEY_NUMPAD4) && !Keyboard.isKeyDown(Keyboard.KEY_NUMPAD2)) {
                    press2 = false;
                    updateDir = true;
                }
                if (!press2 && Keyboard.isKeyDown(Keyboard.KEY_NUMPAD8)) {
                    dir[0] = !dir[0];
                    press2 = true;
                    updateDir = true;
                }
                if (!press2 && Keyboard.isKeyDown(Keyboard.KEY_NUMPAD2)) {
                    dir[1] = !dir[1];
                    press2 = true;
                    updateDir = true;
                }

                if (!press2 && Keyboard.isKeyDown(Keyboard.KEY_NUMPAD4)) {
                    dir[2] = !dir[2];
                    press2 = true;
                    updateDir = true;
                }

                if (!press2 && Keyboard.isKeyDown(Keyboard.KEY_NUMPAD6)) {
                    dir[3] = !dir[3];
                    press2 = true;
                    updateDir = true;
                }

                if (updateDir) {
                    updateDir = false;
                    entitys.forEach(Entity::destroy);
                    entitys.clear();
                    if (dir[0]) {
                        entitys.add(loadEntity("arrow", 1, 6, -2, 0, 0, 0, 0.5f));
                        entitys.add(loadEntity("arrow", 2, 6F, -2, 0, 0, 0, 0.5f));
                        entitys.add(loadEntity("arrow", 3, 6F, -2, 0, 0, 0, 0.5f));
                        entitys.add(loadEntity("arrow", 4, 6, -2, 0, 0, 0, 0.5f));
                        entitys.add(loadEntity("arrow", 5, 6F, -2, 0, 0, 0, 0.5f));
                        entitys.add(loadEntity("arrow", 6, 6F, -2, 0, 0, 0, 0.5f));
                        entitys.add(loadEntity("arrow", 7, 6, -2, 0, 0, 0, 0.5f));
                        entitys.add(loadEntity("arrow", 8, 6F, -2, 0, 0, 0, 0.5f));
                        entitys.add(loadEntity("arrow", 9, 6F, -2, 0, 0, 0, 0.5f));
                    }
                    if (dir[1]) {

                        entitys.add(loadEntity("arrow", 1, 6, 12, 0, 180, 0, 0.5f));
                        entitys.add(loadEntity("arrow", 2, 6F, 12, 0, 180, 0, 0.5f));
                        entitys.add(loadEntity("arrow", 3, 6F, 12, 0, 180, 0, 0.5f));
                        entitys.add(loadEntity("arrow", 4, 6, 12, 0, 180, 0, 0.5f));
                        entitys.add(loadEntity("arrow", 5, 6F, 12, 0, 180, 0, 0.5f));
                        entitys.add(loadEntity("arrow", 6, 6F, 12, 0, 180, 0, 0.5f));
                        entitys.add(loadEntity("arrow", 7, 6, 12, 0, 180, 0, 0.5f));
                        entitys.add(loadEntity("arrow", 8, 6F, 12, 0, 180, 0, 0.5f));
                        entitys.add(loadEntity("arrow", 9, 6F, 12, 0, 180, 0, 0.5f));
                    }
                    if (dir[2]) {

                        entitys.add(loadEntity("arrow", -2, 6, 1, 0, 90, 0, 0.5f));
                        entitys.add(loadEntity("arrow", -2F, 6F, 2, 0, 90, 0, 0.5f));
                        entitys.add(loadEntity("arrow", -2F, 6F, 3, 0, 90, 0, 0.5f));
                        entitys.add(loadEntity("arrow", -2, 6, 4, 0, 90, 0, 0.5f));
                        entitys.add(loadEntity("arrow", -2F, 6F, 5, 0, 90, 0, 0.5f));
                        entitys.add(loadEntity("arrow", -2F, 6F, 6, 0, 90, 0, 0.5f));
                        entitys.add(loadEntity("arrow", -2, 6, 7, 0, 90, 0, 0.5f));
                        entitys.add(loadEntity("arrow", -2F, 6F, 8, 0, 90, 0, 0.5f));
                        entitys.add(loadEntity("arrow", -2F, 6F, 9, 0, 90, 0, 0.5f));
                    }
                    if (dir[3]) {

                        entitys.add(loadEntity("arrow", 12, 6, 1, 0, -90, 0, 0.5f));
                        entitys.add(loadEntity("arrow", 12F, 6F, 2, 0, -90, 0, 0.5f));
                        entitys.add(loadEntity("arrow", 12F, 6F, 3, 0, -90, 0, 0.5f));
                        entitys.add(loadEntity("arrow", 12, 6, 4, 0, -90, 0, 0.5f));
                        entitys.add(loadEntity("arrow", 12F, 6F, 5, 0, -90, 0, 0.5f));
                        entitys.add(loadEntity("arrow", 12F, 6F, 6, 0, -90, 0, 0.5f));
                        entitys.add(loadEntity("arrow", 12, 6, 7, 0, -90, 0, 0.5f));
                        entitys.add(loadEntity("arrow", 12F, 6F, 8, 0, -90, 0, 0.5f));
                        entitys.add(loadEntity("arrow", 12F, 6F, 9, 0, -90, 0, 0.5f));
                    }
                }

                for (int j = 0; j < balles.length; j++) {
                    balles[j].applyForce(new Vector3f(0, -0.01F, 0));
                    if (dir[0])
                        balles[j].applyForce(new Vector3f(0, 0F, 0.001F));
                    if (dir[1])
                        balles[j].applyForce(new Vector3f(0, 0F, -0.001F));
                    if (dir[2])
                        balles[j].applyForce(new Vector3f(0.001F, 0F, 0));
                    if (dir[3])
                        balles[j].applyForce(new Vector3f(-0.001F, 0F, 0));
                    balles[j].update();
                    balles[j].checkEdge(entity);
                }
                break;
            case 23:
                for (int i = 0; i < entities.length; i++) {
                    if (i != 0) {
                        for (int j = 0; j < entities.length; j++) {
                            if (i != j) {
                                Vector3f vector = entities[j].attract(entities[i]);
                                entities[i].applyForce(vector);
                            }
                        }
                        entities[i].update();
                    } else {
                        entities[i].getEntity().increaseRotation(1F, -3F, 5F);
                    }
                }

        }
    }
}