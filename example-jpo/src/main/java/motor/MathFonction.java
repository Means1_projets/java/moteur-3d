package motor;

public interface MathFonction {

    float apply(float x, float z);
}
