package motor;

import fr.eisti.motor.opengl.entity.Entity;
import fr.eisti.motor.opengl.entity.addons.EditColorAddons;
import lombok.Getter;
import lombok.Setter;
import org.lwjgl.util.vector.Vector3f;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class FigureMath {

    private MathFonction fct;
    private float xmin;
    private float xmax;
    private float zmin;
    private float zmax;
    private int n;
    private float xTranslation;
    private float yTranslation;
    private float zTranslation;
    private List<Entity> entities = new ArrayList<>();

    public FigureMath(MathFonction fct, float xmin, float xmax, float zmin, float zmax, int n, float xTranslation, float yTranslation, float zTranslation) {
        this.fct = fct;
        this.xmin = xmin;
        this.xmax = xmax;
        this.zmin = zmin;
        this.zmax = zmax;
        this.n = n;
        this.xTranslation = xTranslation;
        this.yTranslation = yTranslation;
        this.zTranslation = zTranslation;
    }

    public void draw() {
        entities.forEach(Entity::destroy);
        entities.clear();
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                float x = xmin + (xmax - xmin) * (float) i / (float) n;
                float z = zmin + (zmax - zmin) * (float) j / (float) n;
                float y = fct.apply(x, z);

                entities.add(TestScene.getInstance().loadEntity("triangle", xTranslation + x, yTranslation + y, zTranslation + z, 0F, 0f, 0f, 0.01F)
                        .addAddon(new EditColorAddons(new Vector3f((float) i / (float) n, 0, (float) j / (float) n))));
            }
        }
    }
}
